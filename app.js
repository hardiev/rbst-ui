var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const authRouter = require('./routes/auth')
const queryRouter = require('./routes/query')
const digetsRouter = require('./routes/digest')
const clusterRouter = require('./routes/cluster')

var app = express();

app.use(logger('dev'));
app.use(express.json({ limit: '50mb', extended: true }));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', indexRouter)
app.use('/api/users', usersRouter)
app.use('/api/auth', authRouter)
app.use('/api/query', queryRouter)
app.use('/api/digest', digetsRouter)
app.use('/api/cluster', clusterRouter)

if (process.env.NODE_ENV === 'production') {

  app.use((req, res, next) => {
    if (!req.secure) {
      res.redirect(`https://${req.headers.host}${req.url}`)
    }
    next()
  })

  app.use('/', express.static(path.join(__dirname, 'client', 'build')))

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
  })
}

module.exports = app;
