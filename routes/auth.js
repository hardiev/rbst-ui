var express = require('express');
var router = express.Router();
const { check, validationResult } = require('express-validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('config')
const User = require('../models/User')

/* auth route */
router.post(
  '/register',
  [
    check('email', 'Wrong email').isEmail(),
    check('password', 'Min digits - 6!').isLength({ min: 6 })
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req)

      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: 'Некоректні Email або пароль'
        })
      }

      const { email, password, role } = req.body

      const candidate = await User.findOne({ email })

      if (candidate) {
        return res.status(400).json({ message: 'Користувач з зазначеними атрибутами існує!' })
      }

      const hashedPassword = await bcrypt.hash(password, 12)
      const user = new User({ email, password: hashedPassword, role })

      const createdUser = await user.save()

      res.status(201).json({ message: 'Користувача створено!', createdUser })

    } catch (e) {
      res.status(500).json({ message: 'smth wrong, try later' })
    }
  });

router.post(
  '/login',
  [
    check('email', 'Некоректний логін').normalizeEmail({gmail_remove_dots: false}).isEmail(),
    check('password', 'Введіть пароль').exists()
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req)

      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: 'Невірні атрибути доступу...'
        })
      }

      const { email, password } = req.body

      const user = await User.findOne({ email })

      if (!user) {
        return res.status(400).json({ message: 'Користувача не знайдено' })
      }

      const isMatch = await bcrypt.compare(password, user.password)

      if (!isMatch) {
        return res.status(400).json({ message: 'Некоректний пароль, повторіть знову' })
      }

      const token = jwt.sign(
        { userId: user.id },
        config.get('jwtSecret'),
        { expiresIn: '8h' }
      )

      res.json({token, userId: user.id, email, role: user.role || 'authorized'})

    } catch (e) {
      res.status(500).json({ message: 'smth wrong, try later' })
    }
  });

module.exports = router;