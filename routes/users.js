var express = require('express');
var router = express.Router();
const { check, validationResult } = require('express-validator')
const bcrypt = require('bcryptjs')
const auth = require('../middleware/auth.middleware')
const User = require('../models/User')
const Query = require('../models/Query');

/* GET users listing. */
router.post(
  '/',
  auth,
  async (req, res) => {
    try {
      const { id, role } = req.body
      const userRole = await User.findById(id).select("role")
      const result = role === userRole.role ? true : false
      if (result) {
        const users = await User.find({}).select("email role")
        res.status(200).json(users)
      } else {
        res.status(403).json({ message: "Forbidden" })
      }
      // res.status(200).json(userRole)
    } catch (e) {
      res.status(500).json({ message: 'user does not exist && it does not have a role' })
    }
  })

router.get('/:id', auth, async (req, res) => {
  try {
    const user = await User.findById(req.params.id).select('email defaultQuery')
    console.log(req.params.id)
    res.status(200).json(user)
  } catch (e) {
    res.status(500).json({ message: 'smth wrong, getUser Method from users endpoint' })
  }
})

router.post('/update',
  [
    auth,
    check('password')
      .if(check('password').exists())
      .isLength({ min: 6 }).withMessage('it must contain 6 digits')
  ],
  async (req, res) => {

    const errors = validationResult(req)

    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
        message: "Довжина пароля - щонайменше 6 символів"
      })
    }

    const { id, password } = req.body

    let updateObj = req.body

    if (password) {
      var hashedPassword = await bcrypt.hash(password, 12)
      updateObj = {
        ...req.body,
        password: hashedPassword
      }
    }

    try {
      const updatedUser = await User.findByIdAndUpdate(
        id,
        {
          $set: updateObj
        },
        {
          new: true,
          select: 'email role defaultQuery'
        }
      )
      res.status(200).json(updatedUser)
    } catch (e) {
      res.status(500).json({ message: 'smth wrong, updateUser Method from users endpoint ERROR' })
    }
  })

router.get('/delete/:id', auth, async (req, res) => {
  try {
    const user = await User.findByIdAndDelete(req.params.id)
    try {
      await Query.deleteMany({ owner: req.params.id })
    } catch (error) {
      res.status(500).json({ message: 'smth wrong, try later from delete user route (queries reference)' })
    }
    res.json(user)
  } catch (e) {
    res.status(500).json({ message: 'smth wrong, try later from delete user route' })
  }
})

module.exports = router;
