const express = require('express')
const router = express.Router()
const { getArrayOfKeywords } = require('../helpers/keywordMatrix')

// const stops = [
//   '⚡️', '❗️', '🇺🇦', String.fromCharCode(8203, 8203), 'рад', 'shorts', 'cgi', 'and', 'am', 'pm', 'ly',
//   'bit', 'g1', 'nfz0bv', 'go', 'ok', 'br', 'gt', 'lt', 'd1', 'dl', 'nbsp', 'laquo', 'ncov', 'new',
//   'ndash', 'raquo', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс', 'апрел', 'март', 'феврал', 'январ', 'июн', 'выглядит',
//   'июл', 'август', 'сентябр', 'октябр', 'ноябр', 'декабр', 'новост', 'вопросам', 'вопрос', 'бесплатн', 'нравитс',
//   'решен', 'работ', 'сш', 'нов', 'шт'
// ]

// const getArrayOfKeywords = (arr) => {

//   // create array of keywords which are in array
//   const arrayOfKeywordsArrays = arr.reduce((acc, current) => {
//     if (current.hasOwnProperty('keyword')) {
//       return [...acc, [...current.keyword.split(' ')]]
//     } else return acc
//   }, [])

//   // create array of !!!all!!! the keywords
//   const arrayOfKeywords = arr.reduce((accum, current) => {
//     if (current.hasOwnProperty('keyword')) {
//       return [...accum, ...current.keyword.split(' ')]
//     } else return accum
//   }, [])

//   // counting the repeated keywords
//   const countRepeatedKeywords = arrayOfKeywords.reduce((acc, el) => {
//     acc[el] = (acc[el] || 0) + 1;
//     return acc;
//   }, {})

//   // create array for convenient sorting
//   const arrayForSortKeywords = Object.keys(countRepeatedKeywords).reduce((acc, current) => {
//     return [...acc, [current, countRepeatedKeywords[current]]]
//   }, [])

//   // filtering...
//   const filteredKeywords = arrayForSortKeywords.filter(el => !(stops.includes(el[0])) && (el[0].length > 1))

//   // sorting...
//   const sortedKeywordsArray = filteredKeywords.sort((a, b) => {
//     return b[1] - a[1]
//   })

//   // matrix row creator helper
//   const weightMatrixBuildHelper = (firstEl, secondEl, arraysOfKeywords) => {

//     const matrixEl = arraysOfKeywords.reduce((acc, curr) => {
//       if (curr.includes(firstEl) && curr.includes(secondEl)) {
//         acc[0] = firstEl
//         acc[1] = secondEl
//         acc[2] = (acc[2] || 0) + 1
//         return acc
//       } else {
//         acc[0] = firstEl
//         acc[1] = secondEl
//         acc[2] = (acc[2] || 0) + 0
//         return acc
//       }
//     }, [])

//     return matrixEl
//   }

//   // creating matrix of keywords that are present in each document
//   const createMatrix = (n) => {

//     let result = []
//     sortedKeywordsArray.slice(0, n).forEach(el => {
//       const matrixRow = sortedKeywordsArray.slice(0, n).reduce((accum, current) => {
//         const res = weightMatrixBuildHelper(el[0], current[0], arrayOfKeywordsArrays)
//         return [...accum, res]
//       }, [])
//       result.push(matrixRow)
//     })

//     return result
//   }

//   // calculation of the weight of each keyword
//   const keywordWeight = (array) => {
//     const keywordWeightResult = array.reduce((accum, current) => {
//       const keyword = current[0][0]
//       const keywordPersonalIndex = current[0][2]
//       // amount of "0" in matrix row
//       let count = 0
//       current.forEach(el => {
//         if (el[2] === 0) count = count + 1
//       })
//       const weight = Math.sqrt(keywordPersonalIndex) * count
//       return [...accum, [keyword, count, weight]]
//     }, [])

//     return keywordWeightResult.sort((a, b) => {
//       return b[2] - a[2]
//     })
//   }

//   const matrix = createMatrix(100)

//   return {
//     // sortedKeywordsArray: sortedKeywordsArray.slice(0, 100),
//     matrix,
//     keywordWeight: keywordWeight(matrix)
//   }
// }

router.post(
  '/',
  (req, res) => {
    res.status(200).json({
      message: 'OK',
      res: getArrayOfKeywords(req.body),
    })
  }
)

module.exports = router