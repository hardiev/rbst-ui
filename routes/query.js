const { Router } = require('express')
const Query = require('../models/Query')
const auth = require('../middleware/auth.middleware')
const { check, validationResult } = require('express-validator')
const router = Router()

router.post(
  '/create',
  [
    auth,
    check('name', 'Максимальна довжина імені - 100 символів').isString().notEmpty().isLength({ max: 100 })
  ],
  async (req, res) => {
    try {
      const validationErrors = validationResult(req)
      if (!validationErrors.isEmpty()) {
        return res.status(400).json({
          errors: validationErrors.array(),
          message: "Максимальна довжина - 100 смиволів"
        })
      }

      const { name, query, queryDescription } = req.body
      const queryDb = new Query({
        name, query, queryDescription, owner: req.user.userId
      })
      await queryDb.save()
      res.status(201).json(queryDb)
    } catch (e) {
      // console.log(e)
      res.status(500).json({ message: 'smth wrong, try later' })
    }
  })

router.get('/', auth, async (req, res) => {
  try {
    const queries = await Query.find({ owner: req.user.userId })
    // const queries = await Query.find()
    res.json(queries)
  } catch (e) {
    res.status(500).json({ message: 'smth wrong, try later' })
  }
})

router.get('/:id', auth, async (req, res) => {
  try {
    const query = await Query.findById(req.params.id)
    res.json(query)
  } catch (e) {
    res.status(500).json({ message: 'smth wrong, try later' })
  }
})

router.get('/delete/:id', auth, async (req, res) => {
  try {
    const query = await Query.findByIdAndDelete(req.params.id)
    res.json(query)
  } catch (e) {
    res.status(500).json({ message: 'smth wrong, try later from delete route' })
  }
})

router.post('/update', auth, async (req, res) => {
  try {
    const query = await Query.findByIdAndUpdate(
      req.body._id,
      {
        ...req.body,
        // owner: req.user.userId
      },
      {
        new: true
      }
    )
    res.status(200).json(query)
  } catch (e) {
    res.status(500).json({ message: 'smth wrong, try later from update route' })
  }
})

module.exports = router