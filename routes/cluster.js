const express = require('express')
const router = express.Router()
const { getArrayOfKeywords } = require('../helpers/keywordMatrix')

router.post(
  '/',
  (req, res) => {
    const data = getArrayOfKeywords(req.body)
    const clustersKeywords = data.keywordWeight.slice(0, 10).reduce((accum, current) => {
      return [...accum, current[0]]
    }, [])
    // const clustersKeywordsRelations = data.matrix.filter((el) => !clustersKeywords.includes(el[0][0]))
    const clustersKeywordsRelations = data.matrix.reduce((accum, current) => {
      const sorted = current.sort((a, b) => b[2] - a[2])
      const filtered = sorted.filter(el => clustersKeywords.includes(el[1]) && el[0] !== el[1] && el[2] > 0)
      if (filtered.length > 0) {
        return [...accum, filtered]
      } else return accum
    }, [])

    // nodes === clustersFullNodes
    const clustersFullNodes = clustersKeywordsRelations.reduce((accumulator, current) => {
      const termClusterBelong = {
        id: current[0][0],
        group: clustersKeywords.indexOf(current[0][1]) + 1,
        name: current[0][1]
      }
      return [...accumulator, termClusterBelong]
    }, [])

    const nodesForRenderingList = arr => {
      const preprocessedObj = arr.reduce((acc, curr) => {
        return !acc[curr.name] ? {...acc, [curr.name]: [curr.id]} : {...acc, [curr.name]: [...acc[curr.name], curr.id]}
      }, {})
      const preprocessedArr = Object.keys(preprocessedObj).reduce((accum, current) => {
        const bodyArray = [...preprocessedObj[current]]
        const bodyArrayReversed = bodyArray.reverse()
        const bodyArrayWithKeyword = [...bodyArrayReversed, current].reverse()
        return [...accum, bodyArrayWithKeyword]
      }, [])
      const result = preprocessedArr.reduce((accumulator, currentItem) => {
        return [...accumulator, {items: [...currentItem]}]
      }, [])
      return result
    }

    const termsInClusters = clustersKeywordsRelations.reduce((acc, curr) => {
      return [...acc, curr[0][0]]
    }, [])

    const termsInClustersMatrix = data.matrix
      .filter(el => termsInClusters.includes(el[0][0]))
      .reduce((acc, curr) => [...acc, curr.filter(el => termsInClusters.includes(el[1]) && el[2] > 0 && el[0] !== el[1])], [])

    const clustersFullLinksPreProcess = termsInClustersMatrix
      .reduce((accum, current) => [...accum, ...[...current]], [])
      .reduce((acc, curr) => {
        const arrToString = curr.join('')
        const arrToReverseString = Array.of(curr[1], curr[0], curr[2]).join('')
        if (acc.tmp.includes(arrToString)) {
          return {
            tmp: [...acc.tmp],
            result: [...acc.result]
          }
        } else {
          return {
            tmp: [...acc.tmp, arrToReverseString],
            result: [...acc.result, curr]
          }
        }
      }, { tmp: [], result: [] })

    // links === clustersFullLinks
    const clustersFullLinks = clustersFullLinksPreProcess.result
      .reduce((accum, current) => [...accum, { source: current[0], target: current[1], value: current[2] }], [])

    res.status(200).json({
      message: 'OK',
      clustersKeywords,
      clustersFullLinksPreProcess,
      clustersKeywordsRelations,
      clustersFullNodes,
      termsInClustersMatrix,
      clustersFullLinks,
      data: {
        nodes: clustersFullNodes,
        links: clustersFullLinks
      },
      forRendering: nodesForRenderingList(clustersFullNodes)
    })
  }
)

module.exports = router