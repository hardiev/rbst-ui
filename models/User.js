const {Schema, model, Types} = require('mongoose')

const schema = new Schema({
  email: {type: String, required: true, unique: true},
  password: {type: String, required: true},
  queries: [{type: Types.ObjectId, ref: 'Query'}],
  role: { type: String },
  defaultQuery: Schema.Types.Mixed
})

module.exports = model('User', schema)