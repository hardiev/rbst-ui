const { Schema, model, Types } = require('mongoose')

const schema = new Schema({
  name: { type: String, required: true },
  queryDescription: { type: String },
  query: Schema.Types.Mixed,
  owner: { type: Types.ObjectId, ref: 'User' }
})

module.exports = model('Query', schema)