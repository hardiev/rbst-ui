// eslint-disable-next-line
import React, { useState } from 'react';
//create your forceUpdate hook
export const useForceUpdate = () => {
    // eslint-disable-next-line
    const [value, setValue] = useState(0); // integer state
    return () => setValue(value => ++value); // update the state to force render
}