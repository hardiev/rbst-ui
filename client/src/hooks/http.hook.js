import { useState, useCallback, useContext } from 'react'
import { useHistory } from 'react-router'
import { AuthContext } from '../context/AuthContext'

export const useHttp = () => {
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)
  const auth = useContext(AuthContext)
  const history = useHistory()

  const request = useCallback(async (url, method = 'GET', body = null, headers = {}) => {
    setLoading(true)
    try {
      if (body) {
        body = JSON.stringify(body)
        headers['Content-Type'] = 'application/json'
      }
      const response = await fetch(url, { method, body, headers })
      const data = await response.json()

      if (!response.ok && url.search(/rbst-retro/g) !== -1) {
        setLoading(false)
        return data
      }
      
      if (!response.ok) {
        if (response.status === 401) {
          auth.logout()
          history.push('/')
        }
        throw new Error(data.message || 'Custom Error message: Smth wrong')
      }
      setLoading(false)

      return data

    } catch (e) {
      setLoading(false)
      setError(e.message)
      throw e
    }
  }, [auth, history])

  const clearError = useCallback(() => setError(null), [])

  return { loading, request, error, clearError }
}