import { useCallback, useState } from "react"

export const useMessage = () => {
  const [show, setShow] = useState(false)
  const [textMsg, setTextMsg] = useState('')
  const message = useCallback(text => {
    if (text) {
      setShow(true)
      setTextMsg(text)
      // alert('text from messageHook: ', text)
    }
  }, [])

  const removeMessage = useCallback(() => {
    setShow(false)
  }, [])
    
  return { message, show, removeMessage, textMsg }
}