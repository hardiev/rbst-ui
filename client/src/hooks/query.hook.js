
export const useQuery = () => {

  const createShouldArray = (array, field) => {
    const result = array.reduce((acc, current) => {
      acc.push({ term: { [field]: current } })
      return acc
    }, [])
    return result
  }

  const createStringQuery = (obj) => {
    let query = {
      aggs: {
        docs_count_aggregate: {
          date_histogram: {
            field: "pubDate",
            calendar_interval: "day"
          }
        }
      },
      highlight: {
        fields: {
          textBody: {
            type: "unified"
          }
        }
      },
      query: {
        query_string: {
          default_field: "textBody",
          default_operator: "AND",
          // minimum_should_match: "100%",
          query: ''
        }
      }
    }

    let stringQuery = ''
    const stringQueryCreated = Object.keys(obj).reduce((accum, current) => {
      if (current && current === 'title') {
        stringQuery += `title:(${obj[current]})`
      }
      if (current && current === 'textBody') {
        stringQuery += `(${obj[current]})`
      }
      if (current && current === 'source') {
        stringQuery += `source:(${obj[current].join(' OR ')})`
      }
      if (current && current === 'pubDate') {
        stringQuery += `pubDate:[${obj[current].gte} TO ${obj[current].lte}]`
      }
      if (current && current === 'user') {
        stringQuery += `user:("${obj[current].join('" OR "')}")`
      }
      if (current && current === 'event') {
        stringQuery += `event:("${obj[current]}")`
        // stringQuery += `event:(\"${obj[current]}\")`
      }
      if (current && current === 'person') {
        stringQuery += `person:("${obj[current]}")`
        // stringQuery += `person:(\"${obj[current]}\")`
      }
      accum.query.query_string.query = stringQuery
      return accum
    }, query)
    // console.log('FROM QUERY HOOK (stringQueryCreated)', obj, stringQuery, stringQueryCreated)
    return stringQueryCreated
  }

  const createQuery = (obj) => {
    const q = {
      // sort: [
      //   {
      //     pubDate: {
      //       order: "desc"
      //     },
      //   }
      // ],
      aggs: {
        docs_count_aggregate: {
          date_histogram: {
            field: "pubDate",
            calendar_interval: "day"
          }
        }
      },
      query: {
        bool: {
          must: [
            // {
            //   match: {
            //     title: {
            //       query: "Кутузов Станислав"
            //     }
            //   }
            // },
            // {
            //   match: {
            //     textBody: {
            //       query: "Кутузов Станислав"
            //     }
            //   }
            // }
          ],
          filter: [
            // {
            //   bool: {
            //     should: [
            //       {
            //         term: {
            //           source: "Telegram"
            //         }
            //       },
            //       {
            //         term: {
            //           source: "VK"
            //         }
            //       }
            //     ]
            //   }
            // }
          ]
        }
      }
    }
    const queryCreated = Object.keys(obj).reduce((acc, current) => {
      if (current && current === 'title') acc.query.bool.must.push({ match: { [current]: { query: obj[current] } } })
      if (current && current === 'textBody') acc.query.bool.must.push({ match: { [current]: { query: obj[current], operator: 'and' } } })
      if (current && current === 'source') acc.query.bool.filter.push({ bool: { should: createShouldArray(obj[current], current) } })
      if (current && current === 'user') acc.query.bool.filter.push({ bool: { should: createShouldArray(obj[current], current) } })
      if (current && current === 'pubDate') acc.query.bool.filter.push({ range: { [current]: obj[current] } })
      if (current && current === 'event') acc.query.bool.filter.push({ match_phrase: { [current]: obj[current] } })
      if (current && current === 'person') acc.query.bool.filter.push({ match_phrase: { [current]: obj[current] } })
      return acc
    }, q)
    // console.log('FROM QUERY HOOK (queryCreated) ', queryCreated)
    // console.log('FROM QUERY HOOK (createStringQuery) ', createStringQuery(obj))
    return queryCreated
  }

  return { createQuery, createStringQuery }
}