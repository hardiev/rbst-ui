import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useMessage } from '../../hooks/message.hook'
import { useHttp } from '../../hooks/http.hook'
import Button from 'react-bootstrap/esm/Button'
import Spinner from 'react-bootstrap/Spinner'
import Col from 'react-bootstrap/esm/Col'
import Modal from 'react-bootstrap/esm/Modal'
import Row from 'react-bootstrap/esm/Row'
import Form from 'react-bootstrap/Form'
import Toast from 'react-bootstrap/Toast'
import errorLogo from '../../images/error.png'
import { SET_USERS_LIST } from '../../constants'

export const AddUsers = (props) => {

  const { showAddUsers, setShowAddUsers } = props
  const { message, show, removeMessage, textMsg } = useMessage()
  const [form, setForm] = useState({
    email: '',
    password: '',
    role: ''
  })
  const { request, loading, error, clearError } = useHttp()
  const dispatch = useDispatch()
  const users = useSelector(state => state.usersList)

  const changeHandler = event => {
    if (event.target.value) {
      setForm({ ...form, [event.target.name]: event.target.value })
    } else {
      message("Всі поля обов'язкові для заповнення")
    }
  }

  const addClickHandler = async () => {
    if (form.email && form.password && form.role) {
      try {
        const data = await request('/api/auth/register', 'POST', { ...form })
        const { _id, email, role } = data.createdUser
        dispatch({ type: SET_USERS_LIST, payload: [...users, { _id, email, role }] })
        message(data.message)
      } catch (e) { }
      setForm({
        email: '',
        password: '',
        role: ''
      })
    } else {
      message("Всі поля обов'язкові для заповнення")
    }
  }

  useEffect(() => {
    message(error)
    clearError()
  }, [error, message, clearError])

  return (

    <Modal show={showAddUsers} onHide={() => setShowAddUsers(false)} >
      <Modal.Header >
        <Modal.Title>Додати користувача</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group as={Row} controlId="formPlaintextEmail">
            <Form.Label column sm="2">
              Email
            </Form.Label>
            <Col sm="10">
              <Form.Control
                required
                size="sm"
                autoComplete="off"
                placeholder="Зазначте Email"
                value={form.email}
                name="email"
                onChange={changeHandler}
              />
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId="formPlaintextPassword">
            <Form.Label column sm="2">
              Пароль
            </Form.Label>
            <Col sm="10">
              <Form.Control
                required
                size="sm"
                type="password"
                placeholder="Пароль"
                autoComplete="current-password"
                value={form.password}
                name="password"
                onChange={changeHandler}
              />
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId="formPlaintextRole">
            <Form.Label column sm="2">
              Роль
            </Form.Label>
            <Col sm="10">
              <Form.Control
                size="sm"
                as="select"
                name="role"
                value={form.role}
                onChange={changeHandler}
              >
                <option value={''} >Оберіть роль...</option>
                <option value={"authenticated"}>Авторизований користувач</option>
                <option value={"root"}>Необмежені права</option>
              </Form.Control>
            </Col>
          </Form.Group>
        </Form>
        <Toast
          className="error-toast"
          onClose={() => removeMessage()}
          show={show}
          autohide={true}
          delay={1500}
        >
          <Toast.Body>
            <img src={errorLogo} className="rounded mr-2" alt="" width="30" height="30" />
            {textMsg}
          </Toast.Body>
        </Toast>
      </Modal.Body>
      <Modal.Footer>
        {
          loading
            ? <Button variant="success" size="sm" disabled>
              <Spinner
                as="span"
                animation="grow"
                size="sm"
                role="status"
                aria-hidden="true"
              />
              Додати
            </Button>
            : <Button variant="success" size="sm" onClick={() => addClickHandler()}>
              Додати
            </Button>
        }

      </Modal.Footer>
    </Modal>

  )
}