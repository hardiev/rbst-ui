import React, { useContext, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { SET_USERS_LIST } from '../../constants/index'
import { AddUsers } from './AddUsers'
import Alert from 'react-bootstrap/esm/Alert'
import Card from 'react-bootstrap/Card'
import { AuthContext } from '../../context/AuthContext'
import { useHttp } from '../../hooks/http.hook'
import '../../pages/UserCabPage.scss'
import '../SavedQueriesAdmin.scss'
import { Loader } from '../Loader'
import Table from 'react-bootstrap/esm/Table'
import Button from 'react-bootstrap/esm/Button'
import { UserInstance } from './UserInstance'

export const UsersManage = () => {

  const { token } = useContext(AuthContext)
  const { request, loading } = useHttp()
  const storageName = 'userData'
  const { userId, role } = JSON.parse(localStorage.getItem(storageName))
  const [showAddUsers, setShowAddUsers] = useState(false)
  const dispatch = useDispatch()
  const users = useSelector(state => state.usersList)

  useEffect(() => {
    const getUsers = async (userId, userRole) => {
      if (userRole === 'root') {
        try {
          const users = await request(
            `/api/users`,
            'POST',
            {
              id: userId,
              role: userRole
            },
            {
              Authorization: `Bearer ${token}`
            }
          )
          dispatch({ type: SET_USERS_LIST, payload: users })
        } catch (error) {
          console.log('[FROM USEEFFECT USERSMANAGE]', error)
        }
      } else {
        dispatch({ type: SET_USERS_LIST, payload: [] })
      }
    }
    getUsers(userId, role)
  }, [userId, role, request, token, dispatch])

  return (
    <div className="user-cab-info">



      <Card>
        <Card.Header as="h5">Управління користувачами</Card.Header>
        <Card.Body>
          {/* <Card.Title>...</Card.Title> */}
          {
            users && users.length === 0
              ? null
              : <Button
                className="btn-outline-success saved-queries-admin-button"
                size="sm"
                onClick={() => setShowAddUsers(true)}
              >
                + Додати
              </Button>
          }
          <AddUsers showAddUsers={showAddUsers} setShowAddUsers={setShowAddUsers} />
          {
            loading
              ? <Loader />
              : users && users.length === 0
                ? <Alert variant="danger">Доступ обмежено...</Alert>
                : <Table striped bordered hover size="sm">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Mail</th>
                      <th>Роль</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      users.map((el, index) => {
                        return <UserInstance el={el} index={index} key={index} />
                      })
                    }
                  </tbody>
                </Table>
          }
        </Card.Body>
      </Card>



      {/* <h5>Управління користувачами</h5> */}

    </div>
  )
}