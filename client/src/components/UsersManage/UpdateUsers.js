import React, { useContext, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { AuthContext } from '../../context/AuthContext'
import { useHttp } from '../../hooks/http.hook'
import { useMessage } from '../../hooks/message.hook'
import Spinner from 'react-bootstrap/Spinner'
import Button from 'react-bootstrap/esm/Button'
import Modal from 'react-bootstrap/esm/Modal'
import Row from 'react-bootstrap/esm/Row'
import Form from 'react-bootstrap/Form'
import Toast from 'react-bootstrap/Toast'
import Col from 'react-bootstrap/esm/Col'
import errorLogo from '../../images/error.png'
import { SET_USERS_LIST } from '../../constants'

export const UpdateUsers = (props) => {

  const { showUpdate, onHide, el } = props
  const { token } = useContext(AuthContext)
  const { request, loading, error, clearError } = useHttp()
  const { message, show, removeMessage, textMsg } = useMessage()
  const [form, setForm] = useState({
    password: '',
    role: ''
  })
  const dispatch = useDispatch()
  const users = useSelector(state => state.usersList)

  const changeHandler = event => {
    setForm({ ...form, [event.target.name]: event.target.value })
    // if (event.target.value) {
    // } else {
    //   message("Всі поля обов'язкові для заповнення")
    // }
  }

  const updateClickHandler = async (id) => {
    const updateObject = Object.keys(form).reduce((accum, current) => {
      if (form[current].length > 0) {
        return {
          ...accum,
          [current]: form[current]
        }
      } else return accum
    }, {})
    if (Object.entries(updateObject).length) {
      try {
        const updatedUser = await request(
          '/api/users/update',
          'POST',
          {
            ...updateObject,
            id
          },
          {
            Authorization: `Bearer ${token}`
          }
        )
        dispatch({
          type: SET_USERS_LIST,
          // payload: [...users.filter(el => el._id !== updatedUser._id), updatedUser]
          payload: [
            ...users.reduce((acc, curr) => {
              return [...acc, curr._id === updatedUser._id ? updatedUser : curr]
            }, [])
          ]
        })
        message('Атрибути змінено')
        setForm({
          password: '',
          role: ''
        })
      } catch (e) {
        message(error)
        console.log(e, error, 'UPDATE USER BY ID ERROR')
      }
    } else message("Змін не здійснено")
  }

  useEffect(() => {
    const getUser = async (id) => {
      try {
        const user = await request(
          `/api/users/${id}`,
          'GET',
          null,
          {
            Authorization: `Bearer ${token}`
          }
        )
        console.log('[GET USER BY ID]', user)
      } catch (e) {
        console.log(e, 'GET USER BY ID ERROR')
      }
    }
    if (showUpdate) {
      getUser(el._id)
    }
  }, [showUpdate, el._id, request, token])

  useEffect(() => {
    message(error)
    clearError()
  }, [error, message, clearError])

  return (
    <>
      <Modal show={showUpdate} onHide={onHide}>
        <Modal.Header >
          <Modal.Title>Редагування користувача...</Modal.Title>
        </Modal.Header>
        <>
          <Modal.Body>
            <Form>
              <Form.Group as={Row} controlId="formPlaintextEmail">
                <Form.Label column sm="2">
                  Email
                </Form.Label>
                <Col sm="10">
                  <Form.Control
                    type="text"
                    size="sm"
                    autoComplete="off"
                    placeholder={el.email}
                    readOnly
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formPlaintextPassword">
                <Form.Label column sm="2">
                  Пароль
                </Form.Label>
                <Col sm="10">
                  <Form.Control
                    required
                    size="sm"
                    type="password"
                    placeholder="Пароль"
                    autoComplete="current-password"
                    value={form.password}
                    name="password"
                    onChange={changeHandler}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formPlaintextRole">
                <Form.Label column sm="2">
                  Роль
                </Form.Label>
                <Col sm="10">
                  <Form.Control
                    size="sm"
                    as="select"
                    name="role"
                    value={form.role}
                    onChange={changeHandler}
                  >
                    <option value={''} >Оберіть роль...</option>
                    <option value={"authenticated"}>Авторизований користувач</option>
                    <option value={"root"}>Необмежені права</option>
                  </Form.Control>
                </Col>
              </Form.Group>
            </Form>
            <Toast
              className="error-toast"
              onClose={() => removeMessage()}
              show={show}
              autohide={true}
              delay={2000}
            >
              <Toast.Body>
                <img src={errorLogo} className="rounded mr-2" alt="" width="30" height="30" />
                {textMsg}
              </Toast.Body>
            </Toast>
          </Modal.Body>
          <Modal.Footer>
            {
              loading
                ? <Button variant="success" size="sm" disabled>
                  <Spinner
                    as="span"
                    animation="grow"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                  />
                  Оновити
                </Button>
                : <Button variant="success" size="sm" onClick={() => updateClickHandler(el._id)}>
                  {/* <Button variant="success" onClick={() => console.log({form})}> */}
                  Оновити
                </Button>
            }

          </Modal.Footer>
        </>
      </Modal>
    </>
  )
}