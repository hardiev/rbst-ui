import React, { useContext } from 'react'
import { Loader } from '../components/Loader'
import Card from 'react-bootstrap/Card'
import { useQueryContext } from '../context/QueryContext'
import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/esm/Button'
import Alert from 'react-bootstrap/esm/Alert'
import './SavedQueriesAdmin.scss'
import '../pages/UserCabPage.scss'
import { useHttp } from '../hooks/http.hook'
import { AuthContext } from '../context/AuthContext'
import { useDispatch, useSelector } from 'react-redux'
import { SET_QUERIES } from '../constants'
import { FullTextSwitcher } from './FullTextSwitcher/FullTextSwitcher'

export const SavedQueriesListAdmin = () => {
  const queries = useSelector(state => state.queriesList)
  const dispatch = useDispatch()
  const { loading } = useQueryContext()
  const { request, error } = useHttp()
  const { token } = useContext(AuthContext)

  const deleteButtonHandler = (id) => async e => {
    e.preventDefault()
    try {
      const data = await request(
        `/api/query/delete/${id}`,
        'GET',
        null,
        {
          Authorization: `Bearer ${token}`
        }
      )
      // setQueries(queries.filter(el => el._id !== data._id))
      dispatch({
        type: SET_QUERIES,
        payload: queries.filter(el => el._id !== data._id)
      })
      // console.log(data)
    } catch (e) {
      console.log(e, error, 'DELETE QUERY ERROR')
    }
  }

  return (
    <div className="user-cab-info">
      <Card>
        <Card.Header as="h5">Збережені персональні запити</Card.Header>
        <Card.Body>
          {
            loading
              ? <Loader />
              : queries.length === 0
                ? <Alert variant="warning">Збережені запити відсутні</Alert>
                : <Table striped bordered hover size="sm">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Назва запиту</th>
                      <th>Запит</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      queries.map((el, index) => {
                        return <tr key={index}>
                          <td>{index + 1}</td>
                          <td>{el.name}</td>
                          <td>
                            <FullTextSwitcher text={el.query.query.query_string.query} />
                            {/* {el.query.query.query_string.query} */}
                          </td>
                          <td>
                            <Button
                              className="btn-outline-success saved-queries-admin-button"
                              size="sm"
                              onClick={deleteButtonHandler(el._id)}
                            >
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash" viewBox="0 0 16 16">
                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                <path fillRule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                              </svg>
                            </Button>
                          </td>
                        </tr>
                      })
                    }
                  </tbody>
                </Table>
          }
        </Card.Body>
      </Card>
    </div>
  )
}