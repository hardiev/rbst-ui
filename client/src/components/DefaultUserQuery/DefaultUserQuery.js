import React, { useContext } from 'react'
import { AuthContext } from '../../context/AuthContext'
import { useDispatch, useSelector } from 'react-redux'
import { useHttp } from '../../hooks/http.hook'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/esm/Button'
import Spinner from 'react-bootstrap/Spinner'
import { SET_DEFAULT_USER_QUERY } from '../../constants'
import '../../pages/UserCabPage.scss'
import '../SavedQueriesAdmin.scss'
import { FullTextSwitcher } from '../FullTextSwitcher/FullTextSwitcher'

export const DefaultUserQuery = () => {

  const { query: defaultQuery } = useSelector(state => state.defaultUserQuery)
  const dispatch = useDispatch()
  const currentQuery = useSelector(state => state.query) || {}
  const { token } = useContext(AuthContext)
  const { request, loading } = useHttp()
  const { userId: id } = JSON.parse(localStorage.getItem('userData'))

  const updateDefaultQueryHandler = async () => {
    try {
      const updatedUser = await request(
        '/api/users/update',
        'POST',
        {
          id,
          defaultQuery: currentQuery
        },
        {
          Authorization: `Bearer ${token}`
        }
      )
      dispatch({ type: SET_DEFAULT_USER_QUERY, payload: updatedUser?.defaultQuery || currentQuery })
    } catch (error) {
      console.log(error, 'UPDATE USER BY ID ERROR FROM updateDefaultQueryHandler')
    }
  }

  return (
    <div className="user-cab-info">
      <Card>
        <Card.Header as="h5">Запит за замовчанням</Card.Header>
        <Card.Body>
          <Card.Text>
            <FullTextSwitcher text={defaultQuery?.query_string?.query} />
          </Card.Text>
          <Button
            className="btn-outline-success saved-queries-admin-button"
            size="sm"
            onClick={updateDefaultQueryHandler}
            disabled={loading ? true : false}
          >
            {
              loading
                ? <Spinner
                  as="span"
                  animation="grow"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                />
                : null
            }
            Встановити поточний запит за замовчанням
          </Button>
        </Card.Body>
      </Card>
    </div>
  )
}