import React, { useEffect, useRef } from 'react'
import './Aggregation.scss'
import { useNewsContext } from '../../context/NewsContext'
import Chart from 'chart.js'
import config from './config'
import moment from 'moment'
import 'moment/locale/uk'
import uk from "date-fns/locale/uk"
import { registerLocale } from 'react-datepicker'
import { useHistory } from "react-router-dom"
import Container from 'react-bootstrap/esm/Container'

registerLocale("uk", uk);


export const Aggregation = () => {

  const { newsN, queryBody, setQueryBody } = useNewsContext()
  const canvas = useRef()
  let history = useHistory()

  const reducer = (accum, current) => {
    accum.labels.push(moment(current.key_as_string).locale('uk').format('L'))
    accum.dataSet.push(current.doc_count)
    return accum
  }

  const { buckets } = newsN.aggregations.docs_count_aggregate
  const newDataSet = buckets.reduce(reducer, {
    dataSet: [],
    labels: []
  })
  config.data.labels = newDataSet.labels
  config.data.datasets[0].data = newDataSet.dataSet

  useEffect(() => {
    const options = {
      onClick: (e, d) => {
        // console.log(queryBody, 'FROM AFFREGATIONaaa')
        // let detailedQueryBody = { ...queryBody }
        if (d.length > 0) {
          const date = config.data.labels[d[0]._index].split('.').reverse().join('-')
          // console.log(date, '!!!!!!!')
          let detailedQueryBody = {
            ...queryBody,
            query: {
              ...queryBody.query,
              query_string: {
                ...queryBody.query.query_string,
                query: `${queryBody.query.query_string.query} pubDate:[${date} TO ${date}]`
              }
            }
          }
          setQueryBody(detailedQueryBody)
          history.push("/main")
        }
      },
      title: {
        display: false,
        text: 'Custom Chart Title'
      },
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            // stepSize: 2,
            // suggestedMin: 70000,
            // suggestedMax: 3000
          }
        }]
      }
    }
    let ctx = canvas.current.getContext('2d')
    // eslint-disable-next-line
    const chart = new Chart(ctx, { ...config, options })
    // eslint-disable-next-line
  }, [])

  return (
    <div className="col-md-10 ">
      <Container className="app-height" fluid>
        <Container fluid>
          <div className="aggregation-component">
            <h3>Агрегація даних за запитом</h3>
            <canvas
              ref={canvas}
              width="400"
              height="400"
            />
          </div>
        </Container>
      </Container>
    </div>
  )
}