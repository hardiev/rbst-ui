import React, { useState } from 'react'
import './NewsModal.scss';
import Button from 'react-bootstrap/esm/Button'
import Modal from 'react-bootstrap/Modal'
import Image from 'react-bootstrap/Image'
import Accordion from 'react-bootstrap/Accordion'
import { useAccordionToggle } from 'react-bootstrap/AccordionToggle'
import moment from 'moment'
import 'moment/locale/uk'
import uk from "date-fns/locale/uk"
import { registerLocale } from 'react-datepicker'
import Container from 'react-bootstrap/esm/Container'
import Row from 'react-bootstrap/esm/Row'
import Col from 'react-bootstrap/esm/Col'
import { Loader } from '../Loader'
import { BadTonality, GoodTonality } from '../Tonality/Tonality'
import { Citation } from '../Citation/Citation'
import Badge from 'react-bootstrap/esm/Badge';

registerLocale("uk", uk);

export const NewsModal = (props) => {

  const { show, onHide, element } = props

  const [videoToggle, setVideoToggle] = useState(false)
  const [loadingVideo, setLoadingVideo] = useState(false)
  const [fullText, setFullText] = useState(false)

  const createYouTubeLink = sourceLink => {
    const videoId = sourceLink.split('watch?v=')[1]
    const embedLink = `https://www.youtube.com/embed/${videoId}`
    return embedLink
  }

  const VideoWatchToggle = ({ children, eventKey }) => {
    const decoratedOnClick = useAccordionToggle(eventKey, () => {
      setVideoToggle(prev => !prev)
      if (loadingVideo) setLoadingVideo(false)
    })

    return (
      <Button onClick={decoratedOnClick} variant="info" className="show-youtube-btn">
        {children}
      </Button>
    )
  }

  const DefineTonality = ({ tonality }) => {
    if (tonality === 'Good') {
      return (<GoodTonality />)
    }
    else if (tonality === 'Bad') {
      return (<BadTonality />)
    }
    else return tonality
  }

  return (
    <Modal
      className="news-modal-custom"
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {element.title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container fluid>
          <Row>
            <Col>
              {
                element.hasOwnProperty('img')
                  ? <Image className="image-media" src={element.img} rounded />
                  : null
              }
              <p>
                <small>
                  <b><i>{moment(element.pubDate).locale('uk').format('L')}</i></b>
                </small>
                <br />
                <small>
                  <b><i>Джерело:</i></b> {element.source}
                </small>
                <br />
                <small>
                  <b><i>Корисувач:</i></b> {element.user}
                </small>
                <br />
                {
                  element.em && <small>
                    <b><i>Тональність: &nbsp;</i></b>
                    <DefineTonality tonality={element.em} />
                    <br />
                  </small>
                }
                {
                  element.event && <small>
                    <b><i>Пов&apos;язані події: &nbsp;</i></b>
                    {element.event}
                    <br />
                  </small>
                }

              </p>
              {
                !fullText && element.textBody.length > 500
                  ? <div className="news-text-wrapper">
                    <p className="news-text" dangerouslySetInnerHTML={{ __html: `${element.textBody.slice(0, 500)}...` }}></p>
                    <Badge className="news-text-full-switcher" variant="info" onClick={() => setFullText(true)} >Розгорнути</Badge>
                  </div>
                  : <div className="news-text-wrapper">
                    <p className="news-text" dangerouslySetInnerHTML={{ __html: element.textBody }}></p>
                    {
                      element.textBody.length <= 500
                        ? null
                        : <Badge className="news-text-full-switcher" variant="info" onClick={() => setFullText(false)} >Приховати</Badge>
                    }
                  </div>
              }

            </Col>
          </Row>
          {
            element.hasOwnProperty("citation") &&
            <Row>
              <Citation props={element.citation} />
            </Row>
          }

          <Row>
            <Col>
              {element.source === 'YouTube'
                ?
                <Accordion>
                  <VideoWatchToggle as={Button} variant="link" eventKey="0">
                    {!videoToggle ? 'Переглянути відео' : 'Приховати відео'}
                  </VideoWatchToggle>
                  <Accordion.Collapse eventKey="0">
                    <div className="video-div">
                      {
                        videoToggle
                          ?
                          <div className="video-div">
                            {!loadingVideo && <Loader />}
                            <iframe src={createYouTubeLink(element.URL)}
                              width="560"
                              height="315"
                              frameBorder='0'
                              allow='accelerometer; autoplay; clipboard-write; encrypted-media'
                              allowFullScreen
                              title='video'
                              style={{ display: loadingVideo ? 'inline-block' : 'none' }}
                              onLoad={() => setLoadingVideo(true)}
                            />
                          </div>
                          : null
                      }
                    </div>
                  </Accordion.Collapse>
                </Accordion>
                : null
              }
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Закрити</Button>
      </Modal.Footer>
    </Modal>
  )
}