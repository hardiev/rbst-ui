import React, { useState } from 'react'
import Badge from 'react-bootstrap/esm/Badge'
import './FullTextSwitcher.scss'

export const FullTextSwitcher = (props) => {

  const { text } = props
  const [fullText, setFullText] = useState(false)

  return (
    <>
      {
        !fullText && text.length > 100
          ? <>
            {`${text.slice(0, 100)}...`}
            <Badge
              className="btn-full-text-switcher"
              variant="info"
              onClick={() => setFullText(true)}
            >
              Показати весь запит
            </Badge>
          </>
          : <>
            {
              text.length <= 100
                ? null
                : <Badge
                  className="btn-full-text-switcher"
                  variant="info"
                  onClick={() => setFullText(false)}
                >
                  Згорнути
                </Badge>
            }
            {text}
          </>
      }
    </>
  )
}