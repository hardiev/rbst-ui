import React from 'react'
import './MainAnalytics.scss'
import Col from 'react-bootstrap/esm/Col'
import Container from 'react-bootstrap/esm/Container'
import Row from 'react-bootstrap/esm/Row'
import { BrowserRouter, NavLink, Redirect, Route, Switch } from 'react-router-dom'
import { Aggregation } from './Aggregation/Aggregation'
import { Digest } from './Digest/Digest'
import { Maps } from './Maps/Maps'
import { Relations } from './Relations/Relations'
import { InformPortrait } from './InformPortrait/InformPortrait'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChartArea, faMap, faFileSignature, faCubes } from '@fortawesome/free-solid-svg-icons'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { DetailsAggregationPoint } from './Aggregation/DetailsAggregationPoint'
import { DetailsRelationsNode } from './DetailsRelationsNode'

library.add(fab, faFileSignature, faChartArea, faMap, faCubes)

export const MainAnalytics = () => {

  const isFetch = 'isFetch'

  return (
    <>
      <BrowserRouter>
        <Container fluid>
          <Row>
            {/* <Col lg={true}>
              <NavLink to="/stat/aggregation" className="nav-link-custom">
                <div className="nav-analytics">
                  <div>
                    <FontAwesomeIcon icon="chart-area" size="2x" fixedWidth pull="left" />
                    Агрегація
                  </div>
                </div>
              </NavLink>
            </Col> */}
            <Col lg={true}>
              <NavLink to={`/stat/digest/${isFetch}`} className="nav-link-custom">
                <div className="nav-analytics">
                  <FontAwesomeIcon icon="file-signature" size="2x" fixedWidth pull="left" />
                  Дайджест
                </div>
              </NavLink>
            </Col>
            <Col lg={true}>
              <NavLink to="/stat/maps" className="nav-link-custom">
                <div className="nav-analytics">
                  <FontAwesomeIcon icon="map" size="2x" fixedWidth pull="left" />
                  Карта
                </div>
              </NavLink>
            </Col>
            <Col lg={true}>
              <NavLink to="/stat/relations" className="nav-link-custom">
                <div className="nav-analytics">
                  <FontAwesomeIcon icon="cubes" size="2x" fixedWidth pull="left" />
                  Зв'язки персон
                </div>
              </NavLink>
            </Col>
            <Col lg={true}>
              <NavLink to="/stat/informportrait" className="nav-link-custom">
                <div className="nav-analytics">
                  <FontAwesomeIcon icon="cubes" size="2x" fixedWidth pull="left" />
                  Інформпортрет
                </div>
              </NavLink>
            </Col>
          </Row>
        </Container>

        <Switch>
          <>
            <Container fluid>
              {/* <Route path="/stat/aggregation" exact>
                <Aggregation />
              </Route>
              <Route path="/stat/aggregation/details" exact>
                <DetailsAggregationPoint />
              </Route> */}
              <Route path="/stat/digest/:checkFetch" component={Digest} exact>
                {/* <Digest /> */}
              </Route>
              <Route path="/stat/maps" exact>
                <Maps />
              </Route>
              <Route path="/stat/relations" exact>
                <Relations />
              </Route>
              <Route path="/stat/relations/detail" exact>
                <DetailsRelationsNode />
              </Route>
              <Route path="/stat/informportrait" exact>
                <InformPortrait />
              </Route>
              <Redirect to="/stat/aggregation" />
            </Container>
          </>
        </Switch>
      </BrowserRouter>
    </>
  )
}