import React, { useContext, useEffect, useState, useRef } from 'react'
import Container from 'react-bootstrap/esm/Container'
import Row from 'react-bootstrap/esm/Row'
import Col from 'react-bootstrap/esm/Col'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import DropdownMultiselect from "react-multiselect-dropdown-bootstrap"
import './SavedQueriesAdmin.scss'
import { useHttp } from '../hooks/http.hook'
import { AuthContext } from '../context/AuthContext'
import { useQueryContext } from '../context/QueryContext'
import { useQuery } from '../hooks/query.hook'
import DatePicker, { registerLocale } from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css"
import moment from 'moment'
import 'moment/locale/uk'
import uk from "date-fns/locale/uk"

registerLocale("uk", uk);


// function useCreateQuery(queryStrState, setqueryObjState) {
//   const setState = useCallback(() => setqueryObjState((prev) => (
//     { ...prev, query: { db: "all", query: queryStrState.toString() } }
//   )), [queryStrState, setqueryObjState])
//   useEffect(() => {
//     setState()
//   }, [setState])
// }

export const SavedQueriesAdmin = () => {

  const [queryObj, setQueryObj] = useState({})
  // const [isDisabledGeneralSearchButton, setIsDisabledGeneralSearchButton] = useState(true)
  const [isDisabledAdvancedSearchButton, setIsDisabledAdvancedSearchButton] = useState(true)
  const [isDisabledAdvBlock] = useState(false)
  // eslint-disable-next-line
  const [isDisabledGenBlock, setIsDisabledGenBlock] = useState(false)
  // const [advancedQueryString] = useState({
  //   queryInp: '',
  //   docNameInp: '',
  //   docTextInp: '',
  //   sourceInp: '',
  //   toString: function () {
  //     return `${this.queryInp} ${this.docNameInp} ${this.docTextInp} ${this.sourceInp}`.trim()
  //   }
  // })
  const { request, error } = useHttp()
  const { token } = useContext(AuthContext)
  const { queries, setQueries } = useQueryContext()
  // const generalSearchBlockRef = useRef(null)
  // const generalSearchButtonRef = useRef(null)
  // const generalSearchQueryNameInputRef = useRef(null)
  // const generalSearchQueryInputRef = useRef(null)
  const advancedSearchBlockRef = useRef(null)
  const advancedSearchButtonRef = useRef(null)
  const advancedSearchQueryNameInputRef = useRef(null)
  const advancedSearchQueryInputRef = useRef(null)
  const advancedSearchQueryDocNameInputRef = useRef(null)
  const advancedSearchQueryUserInputRef = useRef(null)
  const advancedSearchQuerySourceInputRef = useRef(null)
  const [searchQueryInputObj, setSearchQueryInputObj] = useState({})
  const [queryDescription, setQueryDescription] = useState({})
  const [startDate, setStartDate] = useState(new Date())
  const [endDate, setEndDate] = useState(new Date())
  const { createQuery } = useQuery()

  // useCreateQuery(advancedQueryString, setQueryObj)

  // const isEmptyGenQueryInputs = () => {
  //   if (generalSearchQueryNameInputRef.current.value.length > 0 ||
  //     generalSearchQueryInputRef.current.value.length > 0) return false
  //   if (generalSearchQueryNameInputRef.current.value.length === 0 &&
  //     generalSearchQueryInputRef.current.value.length === 0) return true
  // }

  const isEmptyAdvQueryInputs = () => {
    if (advancedSearchQueryNameInputRef.current.value.length > 0 ||
      advancedSearchQueryInputRef.current.value.length > 0 ||
      advancedSearchQueryDocNameInputRef.current.value.length > 0 ||
      advancedSearchQueryUserInputRef.current.value.length > 0 ||
      advancedSearchQuerySourceInputRef.current.value !== undefined
    ) return false
    if (advancedSearchQueryNameInputRef.current.value.length === 0 &&
      advancedSearchQueryInputRef.current.value.length === 0 &&
      advancedSearchQueryDocNameInputRef.current.value.length === 0 &&
      advancedSearchQueryUserInputRef.current.value.length === 0 &&
      advancedSearchQuerySourceInputRef.current.value === undefined
    ) return true
  }

  const disableGenBlock = func => func(!isEmptyAdvQueryInputs())

  // const disableAdvBlock = func => func(!isEmptyGenQueryInputs())

  // const disableGenButt = func => func(isEmptyGenQueryInputs())

  const disableAdvButt = func => func(isEmptyAdvQueryInputs())

  const queryDescriptionReducer = (acc, current) => {
    acc += `/${queryDescription[current]}/-`
    return acc
  }

  const searchButtonHandler = async event => {
    event.preventDefault()
    console.log('query object!!!', queryObj)
    try {

      const queryDescriptionString = Object.keys(queryDescription).reduce(queryDescriptionReducer, '')
      // setSearchQueryInputObj(createQuery({ textBody: event.target.value }))
      // setQueryObj({ ...queryObj, query: createQuery(searchQueryInputObj), queryDescription: queryDescriptionString })

      const data = await request(
        '/api/query/create',
        'POST',
        // queryObj,
        {
          ...queryObj,
          query: createQuery(searchQueryInputObj),
          // queryDescription: queryDescriptionString
        },
        {
          Authorization: `Bearer ${token}`
        })
      setQueries([...queries, data])
    } catch (e) { }
    setSearchQueryInputObj({})
    setQueryDescription({})
    advancedSearchQueryNameInputRef.current.value = ''
    advancedSearchQueryInputRef.current.value = ''
    advancedSearchQueryDocNameInputRef.current.value = ''
    advancedSearchQueryUserInputRef.current.value = ''
  }

  // const generalQueryNameInputHandler = event => {
  //   disableGenButt(setIsDisabledGeneralSearchButton)
  //   disableAdvBlock(setIsDisabledAdvBlock)
  //   if (event.target.value.trim().length > 0) {
  //     setQueryObj({ ...queryObj, [event.target.name]: event.target.value })
  //   }
  // }

  // const generalQueryInputHandler = event => {
  //   disableGenButt(setIsDisabledGeneralSearchButton)
  //   disableAdvBlock(setIsDisabledAdvBlock)
  //   if (event.target.value.trim().length > 0) {
  //     // setAdvancedQueryString({ ...advancedQueryString, [event.target.name]: event.target.value })
  //     setQueryDescription({ ...queryDescription, textBody: `Text: ${event.target.value}` })
  //     // const queryDescriptionString = Object.keys(queryDescription).reduce(queryDescriptionReducer, '')
  //     setSearchQueryInputObj(createQuery({ textBody: event.target.value }))
  //     // setQueryObj({ ...queryObj, query: searchQueryInputObj, queryDescription: queryDescriptionString })
  //   }
  // }

  const advancedQueryNameInputHandler = event => {
    disableAdvButt(setIsDisabledAdvancedSearchButton)
    disableGenBlock(setIsDisabledGenBlock)
    if (event.target.value.trim().length > 0) {
      setQueryObj({ ...queryObj, [event.target.name]: event.target.value })
    }
    console.log(isEmptyAdvQueryInputs())
    console.log(advancedSearchQueryNameInputRef.current.value)
    console.log(advancedSearchQueryNameInputRef.current.value.length)
    console.log(advancedSearchQueryInputRef.current.value.length)
    console.log(advancedSearchQueryDocNameInputRef.current.value.length)
    console.log(advancedSearchQueryUserInputRef.current.value.length)
    console.log(advancedSearchQuerySourceInputRef.current.value)
  }

  const advancedQueryInputHandler = event => {
    disableAdvButt(setIsDisabledAdvancedSearchButton)
    disableGenBlock(setIsDisabledGenBlock)
    if (event.target.value.trim().length > 0) {
      // setAdvancedQueryString({ ...advancedQueryString, [event.target.name]: event.target.value })
      setQueryDescription({ ...queryDescription, textBody: `У тексті: ${event.target.value}` })
      // const queryDescriptionString = Object.keys(queryDescription).reduce(queryDescriptionReducer, '')
      setSearchQueryInputObj({ ...searchQueryInputObj, textBody: event.target.value })
      // setQueryObj({ ...queryObj, query: searchQueryInputObj, queryDescription: queryDescriptionString })
    }
  }

  const advancedQueryDocNameInputHandler = event => {
    console.log(event.target.getAttribute('data-prefix'))
    disableAdvButt(setIsDisabledAdvancedSearchButton)
    disableGenBlock(setIsDisabledGenBlock)
    if (event.target.value.trim().length >= 0) {
      // setAdvancedQueryString({ ...advancedQueryString, [event.target.name]: `@subject ${event.target.value}` })
      setQueryDescription({ ...queryDescription, title: `Заголовок: ${event.target.value}` })
      // const queryDescriptionString = Object.keys(queryDescription).reduce(queryDescriptionReducer, '')
      setSearchQueryInputObj({ ...searchQueryInputObj, title: event.target.value })
      // setQueryObj({
      //   ...queryObj,
      //   query: searchQueryInputObj,
      //   queryDescription: queryDescriptionString
      // })
    }
  }

  const advancedQueryUserInputHandler = event => {
    disableAdvButt(setIsDisabledAdvancedSearchButton)
    disableGenBlock(setIsDisabledGenBlock)
    if (event.target.value.trim().length >= 0) {
      // setAdvancedQueryString({ ...advancedQueryString, [event.target.name]: `@content ${event.target.value}` })
      setQueryDescription({ ...queryDescription, user: `Користувачі: ${event.target.value.replace(/ /g, ',')}` })
      setSearchQueryInputObj({ ...searchQueryInputObj, user: event.target.value.toLowerCase().split(',') })
    }
  }

  const advancedQuerySourceInputHandler = selected => {
    disableAdvButt(setIsDisabledAdvancedSearchButton)
    disableGenBlock(setIsDisabledGenBlock)
    // if (event.target.value.trim().length >= 0) {
    //   setAdvancedQueryString({ ...advancedQueryString, [event.target.name]: `@source ${event.target.value}` })
    // }
    setQueryDescription({ ...queryDescription, source: `Джерела: ${selected.join(', ')}` })
    setSearchQueryInputObj({ ...searchQueryInputObj, source: selected })
  }

  const inbputStartDateHandler = (date) => {
    disableAdvButt(setIsDisabledAdvancedSearchButton)
    disableGenBlock(setIsDisabledGenBlock)
    setStartDate(date)
    setQueryDescription({ ...queryDescription, pubStartDate: `Поч. дата: ${moment(date).format('YYYY-MM-DD')}` })
    setSearchQueryInputObj({
      ...searchQueryInputObj,
      pubDate: {
        gte: moment(date).format('YYYY-MM-DD'),
        lte: moment(endDate).format('YYYY-MM-DD')
      }
    })
    console.log(moment(date).format('YYYY-MM-DD'))
  }

  const inbputEndDateHandler = (date) => {
    setEndDate(date)
    setQueryDescription({ ...queryDescription, pubEndDate: `Кінц. дата: ${moment(date).format('YYYY-MM-DD')}` })
    setSearchQueryInputObj({
      ...searchQueryInputObj,
      pubDate: {
        gte: moment(startDate).format('YYYY-MM-DD'),
        lte: moment(date).format('YYYY-MM-DD')
      }
    })
  }

  useEffect(() => {
    console.log(error, 'useEffect ERROR')
  }, [error])


  return (
    <Container className="saved-queries-admin" fluid >
      <Row>
        <Col><h5>Збереження персональних запитів</h5></Col>
      </Row>
      <Row>
        <Col>
          {/* <Form>
            <Form.Group
              className={isDisabledGenBlock ? 'disabled-el' : null}
              ref={generalSearchBlockRef}
            >
              <h6>Загальний пошук</h6>
              <Form.Label>&diams; назва запиту:</Form.Label>
              <Form.Control
                ref={generalSearchQueryNameInputRef}
                id="generalQueryName"
                type="text"
                placeholder="Введіть назву"
                size="sm"
                // autocomplete="off"
                name="name"
                // value={queryObj.name}
                onChange={generalQueryNameInputHandler}
              />

              <Form.Label>&diams; пошук у документах:</Form.Label>
              <Form.Control
                ref={generalSearchQueryInputRef}
                type="text"
                placeholder="Введіть пошуковий вираз"
                size="sm"
                name="queryInp"
                // value={queryStringObj.query}
                onChange={generalQueryInputHandler}
              />
              <Button
                ref={generalSearchButtonRef}
                className="btn-outline-success saved-queries-admin-button"
                variant="primary"
                type="submit"
                onClick={searchButtonHandler}
                disabled={isDisabledGeneralSearchButton}
              >
                Зберегти запит
              </Button>
            </Form.Group>
          </Form>
          <hr /> */}
          {/* <Form> */}
          <Form.Group
            className={isDisabledAdvBlock ? 'disabled-el' : null}
            ref={advancedSearchBlockRef}
          >
            <h6>Розширений пошук</h6>
            <Form.Label>&diams; назва запиту:</Form.Label>
            <Form.Control
              ref={advancedSearchQueryNameInputRef}
              id="advancedQueryName"
              type="text"
              placeholder="Введіть назву"
              size="sm"
              name="name"
              onChange={advancedQueryNameInputHandler}
            />

            <Form.Label>&diams; пошук у документах:</Form.Label>
            <Form.Control
              ref={advancedSearchQueryInputRef}
              type="text"
              placeholder="Введіть пошуковий вираз"
              size="sm"
              name="queryInp"
              onChange={advancedQueryInputHandler}
            />

            <Form.Label>&diams; у назві документу:</Form.Label>
            <Form.Control
              ref={advancedSearchQueryDocNameInputRef}
              type="text"
              placeholder="Введіть пошуковий вираз"
              size="sm"
              name="docNameInp"
              data-prefix="@subject"
              onChange={advancedQueryDocNameInputHandler}
            />

            <Form.Label>&diams; за джерелом:</Form.Label>
            {/* <Form.Control
                ref={advancedSearchQuerySourceInputRef}
                as="select"
                size="sm"
                name="sourceInp"
                onChange={advancedQuerySourceInputHandler}
              >
                <option value={'1'} >Оберіть джерело</option>
                <option value={'Twitter'} >Twitter</option>
                <option value={'YouTube'} >YouTube</option>
                <option value={'Telegram'} >Telegram</option>
                <option value={'CONT'} >CONT</option>
                <option value={'Weibo'} >Weibo</option>
                <option value={'Facebook'} >Facebook</option>
                <option value={'LiveJournal'} >LiveJournal</option>
                <option value={'LiveInternet'} >LiveInternet</option>
                <option value={'VK'} >VK</option>
                <option value={'ok'} >Одноклассники</option>
                <option value={'Reddit'} >Reddit</option>
                <option value={'Instagram'} >Instagram</option>
                <option value={'Medium'} >Medium</option>
                <option value={'RuTube'} >RuTube</option>
              </Form.Control> */}
            <DropdownMultiselect
              // className="form-control form-control-sm"
              ref={advancedSearchQuerySourceInputRef}
              options={[
                "Telegram",
                "Twitter",
                "YouTube",
                "CONT",
                "Weibo",
                "Facebook",
                "LiveJournal",
                "LiveInternet",
                "VK",
                "Ok.ru",
                "Reddit",
                "Instagram",
                "Medium",
                "RuTube"
              ]}
              name="source"
              placeholder="Оберіть джерело"
              handleOnChange={advancedQuerySourceInputHandler}
              onClick={e => e.preventDefault()}
            />

            <Form.Label>&diams; за користувачем:</Form.Label>
            <Form.Control
              ref={advancedSearchQueryUserInputRef}
              type="text"
              placeholder="Введіть через пробіл"
              size="sm"
              name="docTextInp"
              onChange={advancedQueryUserInputHandler}
            />

            <Form.Label>&diams; за датою:</Form.Label>
            <br />
            <DatePicker
              dateFormat="dd.MM.yyyy"
              selected={startDate}
              onChange={inbputStartDateHandler}
              locale='uk'
              className="form-control form-control-sm"
              closeOnScroll={true}
              selectsStart
              startDate={startDate}
              endDate={endDate}
            />
            <br />
            <DatePicker
              dateFormat="dd.MM.yyyy"
              selected={endDate}
              onChange={inbputEndDateHandler}
              locale='uk'
              className="form-control form-control-sm"
              closeOnScroll={true}
              selectsEnd
              startDate={startDate}
              endDate={endDate}
              minDate={startDate}
            />

            <br />
            <br />

            <Button
              ref={advancedSearchButtonRef}
              className="btn-outline-success saved-queries-admin-button"
              variant="primary"
              // type="submit"
              onClick={searchButtonHandler}
              disabled={isDisabledAdvancedSearchButton}
            >
              Зберегти запит
              </Button>
          </Form.Group>
          {/* </Form> */}
        </Col>
      </Row>
    </Container>
  )
}