import React from 'react'
import { GoogleMap, LoadScript } from '@react-google-maps/api';
import { Marker } from '@react-google-maps/api';

const containerStyle = {
  width: '100%',
  height: '500px'
};

const center = {
  lat: 48.8105257,
  lng: 29.7681978
};

function MapInstance(props) {
  // eslint-disable-next-line
  const [map, setMap] = React.useState(null)

  // console.log(props.pos)

  // const onLoad = React.useCallback(function callback(map) {
  //   // const bounds = new window.google.maps.LatLngBounds();
  //   // map.fitBounds(bounds);
  //   setMap(map)
  // }, [])

  // const position = {
  //   lat: 48.8105257,
  //   lng: 29.7681978
  // }

  const onLoad = marker => {
    // console.log('marker: ', marker)
  }

  const onUnmount = React.useCallback(function callback(map) {
    setMap(null)
  }, [])

  return (
    <LoadScript
      googleMapsApiKey="AIzaSyCPOQtiJBYj_1znI33w7An8ITIfGyDLxcs"
    >
      <GoogleMap
        mapContainerStyle={containerStyle}
        center={center}
        zoom={5}
        onLoad={onLoad}
        onUnmount={onUnmount}
      >
        {
          props.pos.map((el, i) => {
            const {lat, lng} = el
            return (
              <Marker
                onLoad={onLoad}
                position={{lat, lng}}
                key={i}
              />
            )
          })
        }
        { /* Child components, such as markers, info windows, etc. */}
        <></>
      </GoogleMap>
    </LoadScript>
  )
}

export default React.memo(MapInstance)