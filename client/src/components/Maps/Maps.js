import React from 'react'
import MapInstance from './MapInstance'
import './Maps.scss'
import { useNewsContext } from '../../context/NewsContext'
import Container from 'react-bootstrap/esm/Container'
import Alert from 'react-bootstrap/esm/Alert'

export const Maps = () => {

  const { newsN } = useNewsContext()

  const { hits } = newsN.hits
  const news = hits.reduce((acc, current) => {
    acc.push(current._source)
    return acc
  }, [])

  const geosArr = () => {
    return news
      .filter(el => el.hasOwnProperty('geo'))
      .reduce((acc, current) => {
        return [...acc, ...current.geo.split(', ')]
      }, [])
  }

  const geosArrUnique = Array.from(new Set(geosArr()))

  const geos = geosArrUnique.reduce((acc, current) => {
    const el = current.split(';')
    acc.push({
      location: el[0],
      lat: Number(el[1]),
      lng: Number(el[2])
    })
    return acc
  }, [])

  console.log(geos, 'from MAPS')


  // const position = [
  //   {
  //     lat: 48.8105257,
  //     lng: 29.7681978
  //   },
  //   {
  //     lat: 44.8105257,
  //     lng: 22.7681978
  //   },
  //   {
  //     lat: 43.8105257,
  //     lng: 25.7681978
  //   },
  //   {
  //     lat: 49.6020233,
  //     lng: 34.4871993
  //   }
  // ]

  return (
    <div className="col-md-10 ">
      <Container className="app-height" fluid>
        <Container fluid>
          <div className="maps-component">
            <Alert variant="danger" style={{fontSize: "1.5rem"}}>
              Деякі функціональні можливості - недоступні. Розділ знаходиться у розробці.
            </Alert>
            <h3>Геолокація / згадування у повідомленнях</h3>
            <MapInstance pos={geos} />
          </div>
        </Container>
      </Container>
    </div>
  )
}