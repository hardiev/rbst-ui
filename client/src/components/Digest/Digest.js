import React, { useCallback, useEffect, useState } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import './Digest.scss'
import { useNewsContext } from '../../context/NewsContext'
import { DigestItems } from './DigestItems'
import { DigestsList } from './DigestsList'
import { useHttp } from '../../hooks/http.hook'
import config from '../../config/config'
import Container from 'react-bootstrap/esm/Container'
import { useSelector } from 'react-redux'

export const Digest = (props) => {

  const { checkFetch } = props.match.params
  const defaultQueryFromReduxState = useSelector(state => state.defaultUserQuery)
  const [digestData, setDigestData] = useState([])
  const [digestLoading, setDigestLoading] = useState(true)
  const { newsN, queryBody } = useNewsContext()
  const { request } = useHttp()
  const authString = 'Basic ' + btoa(`${config.elasticUser}:${config.elasticPass}`)
  const options = {
    "method": "POST",
    "headers": {
      "Content-Type": "application/json",
      "Authorization": authString
    }
  }

  if (Object.keys(newsN).length > 0) {
    const { hits } = newsN.hits
    var newsNew = hits.reduce((acc, current) => {
      acc.push(current._source)
      return acc
    }, [])
  }

  const newQueryBody = queryBody ? { ...queryBody } : { ...defaultQueryFromReduxState }
  const keyOfDigestArr = digestData.reduce((acc, current) => {
    const { hits } = current.value.hits
    let keyInfo = {}
    const keyInfoFilter = hits.filter(el => el._source.title.length > 20 &&
      !acc.uniq.includes(el._source.title))

    if (!keyInfoFilter[0]) {
      return acc
    } else {
      keyInfo = keyInfoFilter[0]._source
    }

    const keyObject = {
      keyInfo,
      keyDigestInfo: hits
    }
    if (!acc.uniq.includes(keyObject.keyInfo.title)) acc.uniq.push(keyObject.keyInfo.title)
    acc.data.push(keyObject)
    return acc
  }, { data: [], uniq: [] })

  const getDigest = useCallback(async () => {
    try {
      const data = await request('/api/digest', 'POST', [...newsNew])
      const digestRequestsNew = data.res.keywordWeight
        .slice(0, 15)
        .reduce((acc, current) => {
          const [keyword] = current
          const body = {
            ...newQueryBody,
            query: {
              query_string: {
                ...newQueryBody.query.query_string,
                query: `${newQueryBody.query.query_string.query} keyword: "${keyword}"`
              }
            }
          }
          acc.push(body)
          return acc
        }, [])

      const requests = digestRequestsNew.map(async el => {
        const req = await fetch(
          `${config.elasticURL}${config.elasticIndexes}/_search?size=300`,
          // 'http://91.103.255.42:9200/rbst-retro,rbst-current/_search?size=300',
          {
            ...options,
            body: JSON.stringify(el)
          }
        ).then(d => d.json())
        return req
      })

      const resultArr = await Promise.allSettled(requests)
      setDigestData(resultArr)
      // setCurrentDigestInfo(keyOfDigestArr)
      setDigestLoading(false)

    } catch (e) { }
    // eslint-disable-next-line
  }, [])

  useEffect(() => {
    if (checkFetch === 'isFetch') {
      getDigest()
    } else {
      setDigestLoading(false)
    }
  }, [getDigest, checkFetch])

  /**
   * Digest UI
   */

  return (
    <div className="col-md-10 ">
      <Container className="app-height" fluid>
        <Container fluid>
          <div className="digest-component">
            <h3>Дайджест за поточним запитом</h3>
            <BrowserRouter>
              <Switch>
                <Route
                  path="/digest/isFetch"
                  render={(props) => <DigestsList digestData={keyOfDigestArr.data} digestLoading={digestLoading} {...props} />}
                  exact
                />
                <Route
                  path="/digestItems/:id"
                  render={(props) => <DigestItems digestData={keyOfDigestArr.data} {...props} />}
                  exact />
              </Switch>
            </BrowserRouter>
          </div>
        </Container>
      </Container>
    </div>
  )
}