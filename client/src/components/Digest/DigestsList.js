import React from 'react'
import Col from 'react-bootstrap/esm/Col'
import Container from 'react-bootstrap/esm/Container'
import ListGroup from 'react-bootstrap/esm/ListGroup'
import OverlayTrigger from 'react-bootstrap/esm/OverlayTrigger'
import Row from 'react-bootstrap/esm/Row'
import Tooltip from 'react-bootstrap/esm/Tooltip'
import { NavLink } from 'react-router-dom'
import { Loader } from '../Loader'

export const DigestsList = (props) => {

  const { digestLoading, digestData } = props
  const isImageExists = element => {
    return element.hasOwnProperty("img") ? true : false
  }

  return (
    <Container fluid>
      <ListGroup variant="flush">
        {
          digestLoading
            ? <Loader />
            : digestData.map((el, index) => {
              return <ListGroup.Item key={index} >
                <Row>
                  <Col>
                    <OverlayTrigger
                      placement="right"
                      overlay={
                        <Tooltip id="tooltip-right">
                          Кількість повідомлень у дайджесті
                        </Tooltip>
                      }
                    >
                      <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" fill="currentColor" className="bi bi-card-list" viewBox="0 0 16 16">
                        <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z" />
                        <path d="M5 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 5 8zm0-2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0 5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-1-5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zM4 8a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zm0 2.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z" />
                      </svg>
                    </OverlayTrigger>
                    <div>{el.keyDigestInfo.length}</div>
                  </Col>
                  <Col xs={8} className="digest-key-body">
                    <NavLink className="digest-key-header" to={`/digestItems/${index}`}>
                      <h5>{el.keyInfo.title}</h5>
                    </NavLink>
                    <div>{el.keyInfo.textBody.slice(0, 300)}</div>
                  </Col>
                  <Col>
                    {
                      isImageExists(el.keyInfo)
                        ? <img
                          width={130}
                          // height={64}
                          className="mr-3"
                          src={el.keyInfo.img}
                          alt="Generic placeholder"
                        />
                        : null
                    }
                  </Col>
                </Row>
              </ListGroup.Item>
            })
        }
      </ListGroup>
    </Container>
  )
}