import React, { useState } from 'react'
import ListGroup from 'react-bootstrap/esm/ListGroup'
import { NewsModal } from '../NewsModal/NewsModal'

export const DigestInformationItem = (props) => {

  const [modalShow, setModalShow] = useState(false)

  return (
    <>
      <ListGroup.Item onClick={() => setModalShow(true)}>
        {props.props._source.title}
      </ListGroup.Item>
      <NewsModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        element={props.props._source}
      />

    </>
  )
}