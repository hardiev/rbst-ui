import React from 'react'
import Alert from 'react-bootstrap/esm/Alert'
import ListGroup from 'react-bootstrap/esm/ListGroup'
import { DigestInformationItem } from './DigestInformationItem'
import uniqBy from 'lodash/uniqBy'

export const DigestInformation = (props) => {

  const uniqDigestList = uniqBy(props.digestList, '_score')

  return (
    <div>
      <ListGroup>
        {
          props.digestList === 'NoData'
            ? <Alert variant="info">Сюжет не обрано</Alert>
            : uniqDigestList
              .filter(el => el._source.title.length > 15)
              .map((el, i) => {
                return (
                  <DigestInformationItem key={i} props={el} />
                )
              })
        }
      </ListGroup>
    </div>
  )
}