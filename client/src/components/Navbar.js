import React, { useContext } from 'react'
import config from '../config/config'
import { NavLink, useHistory } from 'react-router-dom'
import { AuthContext } from '../context/AuthContext'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faAddressCard,
  faChartBar,
  faUsersCog,
  faSignOutAlt,
  faChartPie,
  faInfoCircle,
  faSyncAlt,
  faFileSignature,
  faMap,
  faCubes,
  faCogs
} from '@fortawesome/free-solid-svg-icons'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { Brand } from './Brand'

library.add(fab,
  faAddressCard,
  faChartBar,
  faUsersCog,
  faSignOutAlt,
  faChartPie,
  faInfoCircle,
  faSyncAlt,
  faFileSignature,
  faMap,
  faCubes,
  faCogs)

export const Navbar = () => {
  const history = useHistory()
  const auth = useContext(AuthContext)

  const logoutHandler = (event) => {
    event.preventDefault()
    auth.logout()
    history.push('/')
  }

  const isFetch = 'isFetch'

  return (
    <>
      <nav className="bg-light col-md-2 d-md-block d-none sidebar">
        <Brand />
        <div className="sidebar-sticky">
          <ul className="nav flex-column nav-main-page">
            <li className="nav-item">
              <a className="nav-link" href="/" rel="noopener noreferrer">
                <FontAwesomeIcon icon="sync-alt" size="lg" fixedWidth pull="left" />
                Запит за замовчанням
              </a>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/main">
                <FontAwesomeIcon icon="address-card" size="lg" fixedWidth pull="left" />
                Головна
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/usercab">
                <FontAwesomeIcon icon="users-cog" size="lg" fixedWidth pull="left" />
                Кабінет користувача
              </NavLink>
            </li>
            {/* <li className="nav-item">
              <NavLink className="nav-link" to="/stat">
                <FontAwesomeIcon icon="chart-bar" size="lg" fixedWidth pull="left" />
              Аналітичні інструменти
              </NavLink>
            </li> */}
            <li className="nav-item">
              <NavLink className="nav-link" to="/aggregation">
                <FontAwesomeIcon icon="chart-bar" size="lg" fixedWidth pull="left" />
                Агрегація повідомлень
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to={`/digest/${isFetch}`}>
                <FontAwesomeIcon icon="file-signature" size="lg" fixedWidth pull="left" />
                Дайджест
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/maps">
                <FontAwesomeIcon icon="map" size="lg" fixedWidth pull="left" />
                Карти
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/relations">
                <FontAwesomeIcon icon="cubes" size="lg" fixedWidth pull="left" />
                Зв'язки персон
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/informportrait">
                <FontAwesomeIcon icon="cogs" size="lg" fixedWidth pull="left" />
                Інформпортрет
              </NavLink>
            </li>
            <li className="nav-item">
              <a className="nav-link" href={`${config.kibanaURL}/app/home#/`} target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon icon="chart-pie" size="lg" fixedWidth pull="left" />
                Візуалізація даних (Kibana)
              </a>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/help">
                <FontAwesomeIcon icon="info-circle" size="lg" fixedWidth pull="left" />
                Допомога
              </NavLink>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/logout" onClick={logoutHandler}>
                <FontAwesomeIcon icon="sign-out-alt" size="lg" fixedWidth pull="left" />
                Вийти
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </>
  )
}