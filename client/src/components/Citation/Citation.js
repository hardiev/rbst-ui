import React from 'react'
import Table from 'react-bootstrap/Table'

export const Citation = ({ props }) => {

  return (
    <div>
      <h5>Цитати</h5>
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Автор</th>
            <th>Текст</th>
          </tr>
        </thead>
        <tbody>
          {
            props.map((el, i) => {
              return (
                <tr key={i}>
                  <td>{i + 1}</td>
                  <td>{el.acit}</td>
                  <td>{el.cit}</td>
                </tr>
              )
            })
          }
        </tbody>
      </Table>
    </div>
  )
}