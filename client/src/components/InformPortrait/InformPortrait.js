import React from 'react'
import './InformPortrait.scss'
import Container from 'react-bootstrap/esm/Container'
import { ArcDiagram } from './ArcDiagram'

export const InformPortrait = () => {

  return (
    <div className="col-md-10 ">
      <Container className="app-height" fluid>
        <Container fluid>
          <div className="informportrait-component">
            <h3>Зв'язки за ключовими словами (інформаційний портрет)</h3>
            <ArcDiagram />
          </div>
        </Container>
      </Container>
    </div>
  )
}