import React, { useEffect, useRef, useState } from 'react'
import * as d3 from 'd3'
import Badge from 'react-bootstrap/esm/Badge'
import Button from 'react-bootstrap/esm/Button'
import { useHttp } from '../../hooks/http.hook'
import { useNewsContext } from '../../context/NewsContext'
import { Loader } from '../Loader'
import { useHistory } from 'react-router'
import Container from 'react-bootstrap/esm/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import ListGroup from 'react-bootstrap/ListGroup'



export const ArcDiagram = () => {

  const diagramRef = useRef(null)
  const sortNameButtonRef = useRef(null)
  const sortGroupButtonRef = useRef(null)
  const sortDegreeButtonRef = useRef(null)
  const [dataForRendering, setDataForRendering] = useState([])
  const { request, loading } = useHttp()
  const { newsN, queryBody, setQueryBody } = useNewsContext()
  let history = useHistory()

  const { hits } = newsN.hits
  const newsSource = hits.reduce((acc, current) => {
    acc.push(current._source)
    return acc
  }, [])

  const nodeClickHandler = data => {

    const detailedQueryBody = {
      ...queryBody,
      query: {
        ...queryBody.query,
        query_string: {
          ...queryBody.query.query_string,
          query: `${queryBody.query.query_string.query} keyword:"${data}"`
        }
      }
    }

    // console.log('FROM ARCDIAGRAM', data)
    setQueryBody(detailedQueryBody)
    history.push("/main")
  }

  const keyWordsItemsJSX = dataForRendering.reduce((acc, curr) => {
    const groupElement = curr.items.map((elem, i) => {
      if (i === 0) {
        return (
          <ListGroup.Item
            as="li"
            active
            data-item={elem}
            key={elem + i}
            className="keywords-kw"
            onClick={e => nodeClickHandler(e.target.getAttribute('data-item'))}
          >
            {elem}
          </ListGroup.Item>
        )
      } else {
        return (
          <ListGroup.Item
            as="li"
            data-item={elem}
            className="keywords-item"
            key={elem + i}
            onClick={e => nodeClickHandler(e.target.getAttribute('data-item'))}
          >
            {elem}
          </ListGroup.Item>
        )
      }
    })
    return [...acc, ...groupElement]
  }, [])

  useEffect(() => {

    const createDiagram = (data) => {
    const nodes = data.nodes.map(({ id, group }) => ({
      id,
      sourceLinks: [],
      targetLinks: [],
      group
    }));

    const nodeById = new Map(nodes.map(d => [d.id, d]));

    const links = data.links.map(({ source, target, value }) => ({
      source: nodeById.get(source),
      target: nodeById.get(target),
      value
    }));

    for (const link of links) {
      const { source, target } = link;
      source.sourceLinks.push(link);
      target.targetLinks.push(link);
    }
    // setGraph({nodes, links})
    const graph = { nodes, links }

    const color = d3.scaleOrdinal(graph.nodes.map(d => d.group).sort(d3.ascending), d3.schemeCategory10)

    const step = 15

    const margin = ({ top: 20, right: 20, bottom: 20, left: 100 })

    const height = (data.nodes.length - 1) * step + margin.top + margin.bottom

    const y = d3.scalePoint(graph.nodes.map(d => d.id).sort(d3.ascending), [margin.top, height - margin.bottom])

    function arc(d) {
      const y1 = d.source.y;
      const y2 = d.target.y;
      const r = Math.abs(y2 - y1) / 2;
      return `M${margin.left},${y1}A${r},${r} 0,0,${y1 < y2 ? 1 : 0} ${margin.left},${y2}`;
    }

    const svg = d3.select(diagramRef.current)
      .append("svg")
      .attr("viewBox", [-30, 0, 850, height])

    svg.append("style").text(`
      .hover path {
      stroke: #ccc;
      }

      .hover text {
      fill: #ccc;
      }

      .hover g.primary text {
      fill: black;
      font-weight: bold;
      }

      .hover g.secondary text {
      fill: #333;
      }

      .hover path.primary {
      stroke: #333;
      stroke-opacity: 1;
      }
      `);

    const label = svg.append("g")
      .attr("font-family", "sans-serif")
      .attr("font-size", 16)
      .attr("text-anchor", "end")
      .selectAll("g")
      .data(graph.nodes)
      .join("g")
      .attr("transform", d => `translate(${margin.left},${d.y = y(d.id)})`)
      .call(g => g.append("text")
        .attr("x", -6)
        .attr("dy", "0.35em")
        .attr("fill", d => d3.lab(color(d.group)).darker(2))
        .text(d => d.id))
      .call(g => g.append("circle")
        .attr("r", 3)
        .attr("fill", d => color(d.group)))
      .on("click", (e, d) => console.log(d));

    const path = svg.insert("g", "*")
      .attr("fill", "none")
      .attr("stroke-opacity", 0.6)
      .attr("stroke-width", 1.5)
      .selectAll("path")
      .data(graph.links)
      .join("path")
      .attr("stroke", d => d.source.group === d.target.group ? color(d.source.group) : "#aaa")
      .attr("d", arc);

    const overlay = svg.append("g")
      .attr("fill", "none")
      .attr("pointer-events", "all")
      .selectAll("rect")
      .data(graph.nodes)
      .join("rect")
      .attr("width", margin.left + 40)
      .attr("height", step)
      .attr("y", d => y(d.id) - step / 2)
      .on("mouseover", (e, d) => {
        svg.classed("hover", true);
        label.classed("primary", n => n === d);
        label.classed("secondary", n => n.sourceLinks.some(l => l.target === d) || n.targetLinks.some(l => l.source === d));
        path.classed("primary", l => l.source === d || l.target === d).filter(".primary").raise();
      })
      .on("mouseout", d => {
        svg.classed("hover", false);
        label.classed("primary", false);
        label.classed("secondary", false);
        path.classed("primary", false).order();
      })
      .on("click", (e, d) => nodeClickHandler(d.id));


    d3.select(sortNameButtonRef.current)
      .on("click", function () {
        console.log('[from button]')
        y.domain(graph.nodes.sort((a, b) => {
          if (a.id > b.id) return 1
          if (a.id < b.id) return -1
          // a должно быть равным b
          return 0;
        }).map(d => d.id));

        const t = svg.transition()
          .duration(750);

        label.transition(t)
          .delay((d, i) => i * 20)
          .attrTween("transform", d => {
            const i = d3.interpolateNumber(d.y, y(d.id));
            return t => `translate(${margin.left},${d.y = i(t)})`;
          });

        path.transition(t)
          .duration(750 + graph.nodes.length * 20)
          .attrTween("d", d => () => arc(d));

        overlay.transition(t)
          .delay((d, i) => i * 20)
          .attr("y", d => y(d.id) - step / 2);
      })

    d3.select(sortGroupButtonRef.current)
      .on("click", function () {
        console.log('[from button]')
        y.domain(graph.nodes.sort((a, b) => a.group - b.group).map(d => d.id));

        const t = svg.transition()
          .duration(750);

        label.transition(t)
          .delay((d, i) => i * 20)
          .attrTween("transform", d => {
            const i = d3.interpolateNumber(d.y, y(d.id));
            return t => `translate(${margin.left},${d.y = i(t)})`;
          });

        path.transition(t)
          .duration(750 + graph.nodes.length * 20)
          .attrTween("d", d => () => arc(d));

        overlay.transition(t)
          .delay((d, i) => i * 20)
          .attr("y", d => y(d.id) - step / 2);
      })

    d3.select(sortDegreeButtonRef.current)
      .on("click", function () {
        console.log('[from button]')
        y.domain(graph.nodes.sort((a, b) => b.targetLinks.length - a.targetLinks.length).map(d => d.id));

        const t = svg.transition()
          .duration(750);

        label.transition(t)
          .delay((d, i) => i * 20)
          .attrTween("transform", d => {
            const i = d3.interpolateNumber(d.y, y(d.id));
            return t => `translate(${margin.left},${d.y = i(t)})`;
          });

        path.transition(t)
          .duration(750 + graph.nodes.length * 20)
          .attrTween("d", d => () => arc(d));

        overlay.transition(t)
          .delay((d, i) => i * 20)
          .attr("y", d => y(d.id) - step / 2);
      })

    // y.domain(graph.nodes.sort(viewof order.value).map(d => d.id));
    y.domain(graph.nodes.sort(d3.ascending).map(d => d.id));

    const t = svg.transition()
      .duration(750);

    label.transition(t)
      .delay((d, i) => i * 20)
      .attrTween("transform", d => {
        const i = d3.interpolateNumber(d.y, y(d.id));
        return t => `translate(${margin.left},${d.y = i(t)})`;
      });

    path.transition(t)
      .duration(750 + graph.nodes.length * 20)
      .attrTween("d", d => () => arc(d));

    overlay.transition(t)
      .delay((d, i) => i * 20)
      .attr("y", d => y(d.id) - step / 2);

  }

    const getData = async () => {
      const res = await request('/api/cluster', 'POST', [...newsSource])
      const { data, forRendering } = await res
      setDataForRendering(forRendering)
      console.log('[from ArcDiagram]', res)
      // setData(data)
      createDiagram(data)
    }
    getData()
    // createDiagram()
    // eslint-disable-next-line
  }, [request])

  return (
    <>

      {
        loading
          ? <Loader />
          : <>
            <Container fluid>
              <Row>
                <Col>
                  <h5>
                    <Badge variant="light" className="badge-order" >Сортування:</Badge>
                    <Button variant="secondary" size="sm" className="diagram-badge-order" ref={sortNameButtonRef}>Назва</Button>
                    <Button variant="secondary" size="sm" className="diagram-badge-order" ref={sortGroupButtonRef}>Група</Button>
                    <Button variant="secondary" size="sm" className="diagram-badge-order" ref={sortDegreeButtonRef}>Індекс зв'язку</Button>
                  </h5>
                  <div ref={diagramRef}>
                  </div>
                </Col>
                <Col>
                  <ListGroup as="ul" className="keywords-list">
                    {keyWordsItemsJSX}
                  </ListGroup>
                </Col>
              </Row>
            </Container>
          </>
      }
    </>
  )
}