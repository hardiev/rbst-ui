import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useLocation } from 'react-router-dom'
import { SavedQueries } from './SavedQueries'
import { useQuery } from '../hooks/query.hook'
import { SET_HEADER_QUERY_INPUT, SET_QUERY_MODEL } from '../constants'
import './Header.scss'

export const Header = ({ search }) => {
  const [collapse, setCollapse] = useState(true)
  const location = useLocation()
  const { createStringQuery } = useQuery()
  const dispatch = useDispatch()
  const searchInputFromReduxState = useSelector(state => state.headerQueryInput)
  const searchInputEl = useRef({})

  const toggleButtonHandler = () => {
    setCollapse(!collapse)
  }

  const searchButtontHandler = (event) => {
    event.preventDefault()
    dispatch({ type: SET_QUERY_MODEL, payload: { textBody: searchInputFromReduxState } })
    search(createStringQuery({ textBody: searchInputFromReduxState }))
  }

  const changeSearchInputHandler = (event) => {
    dispatch({ type: SET_HEADER_QUERY_INPUT, payload: event.target.value })
  }

  useEffect(() => {
    searchInputEl.current.value = searchInputFromReduxState || ''
  }, [searchInputFromReduxState])

  return (
    <header className="header-main-container">
      <nav className="navbar navbar-expand-md navbar-dark bg-dark header-custom">
        <button onClick={toggleButtonHandler} className={collapse ? 'navbar-toggler collapsed' : 'navbar-toggler'} type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-expanded="true" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className={collapse ? 'navbar-collapse collapse' : 'navbar-collapse'} id="navbarCollapse" >
          <form className="form-inline mt-2 mt-md-0 header-search-form">
            {
              location.pathname === '/main'
                ? <>
                  <input className="form-control mr-sm-2 header-search-form-input"
                    type="text"
                    placeholder="Введіть пошуковий запит"
                    aria-label="Search"
                    ref={searchInputEl}
                    onChange={changeSearchInputHandler}
                  />
                  <button
                    className="btn btn-outline-success my-2 my-sm-0"
                    type="submit"
                    onClick={searchButtontHandler}
                  >
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
                      <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                    </svg>
                  </button>
                </>
                : null
            }
          </form>
          {
            location.pathname === '/main'
              ? <>
                <SavedQueries search={search}>

                </SavedQueries>
              </>
              : null
          }
        </div>
      </nav>
    </header>
  )
}