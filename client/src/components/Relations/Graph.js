import React, { useEffect, useRef } from 'react'
import * as d3 from 'd3'
// import data from './data.json'

export const Graph = (props) => {

  const { data, handler } = props

  const myRef = useRef()

  const links = data.links.map(d => Object.create(d))
  const nodes = data.nodes.map(d => Object.create(d))

  useEffect(() => {

    const height = 800
    const width = 800

    // const color = (d) => {
    //   const scale = d3.scaleOrdinal(d3.schemeCategory10);
    //   const c = d => scale(d.group);
    //   return c
    // }
    const color = 'green'
    
    // console.log(color, 'COLOR');
    let drag = simulation => {

      function dragstarted(event) {
        if (!event.active) simulation.alphaTarget(0.3).restart();
        event.subject.fx = event.subject.x;
        event.subject.fy = event.subject.y;
      }

      function dragged(event) {
        event.subject.fx = event.x;
        event.subject.fy = event.y;
      }

      function dragended(event) {
        if (!event.active) simulation.alphaTarget(0);
        event.subject.fx = null;
        event.subject.fy = null;
      }

      return d3.drag()
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended);
    }

    const simulation = d3.forceSimulation(nodes)
      .force("link", d3.forceLink(links).id(d => d.id).distance(130))
      .force("charge", d3.forceManyBody().strength(0))
      .force("center", d3.forceCenter(width / 2, height / 3))

    const svg = d3.select(myRef.current)
      .append("svg")
      .attr("viewBox", [0, 0, width, height])

    const link = svg.append("g")
      .attr("stroke", "#999")
      .attr("stroke-opacity", 0.6)
      .selectAll("line")
      .data(links)
      .join("line")
      .attr("stroke-width", d => Math.sqrt(d.weight))

    const node = svg.append("g")
      // .attr("stroke", "#fff")
      // .attr("stroke-width", 1.5)
      .selectAll(".node")
      .data(nodes)
      .join("g")
      .attr("class", "node")
      // .attr("fill", color)
      .call(drag(simulation))
      .on('click', (e, d) => {
        handler(data.nodes[d.index].id)
        // console.log(data.nodes[d.index], 'd3 node clicked')
        
      })

    node.append("circle")
      .attr("r", 5)
      .attr("fill", color)

    node.append("text")
      .text(function (d) {
        return d.id;
      })
      .style('fill', '#000')
      .style('font-size', '12px')
      .attr('x', 6)
      .attr('y', 3);

    // node.append("title")
    //   .text(d => d.id)

    simulation.on("tick", () => {
      link
        .attr("x1", d => d.source.x)
        .attr("y1", d => d.source.y)
        .attr("x2", d => d.target.x)
        .attr("y2", d => d.target.y)

      node
        .attr("transform", d => `translate(${d.x}, ${d.y})`);

      // node
      //   .attr("cx", d => d.x)
      //   .attr("cy", d => d.y)


    });

    // invalidation.then(() => simulation.stop())

  })

  return (
    <div
      ref={myRef}
    ></div>
  )
}