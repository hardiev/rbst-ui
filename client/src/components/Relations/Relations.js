import React from 'react'
import { Graph } from './Graph'
import './Relations.scss'
import { useNewsContext } from '../../context/NewsContext'
import { useHistory } from "react-router-dom"
import Container from 'react-bootstrap/esm/Container'
import Alert from 'react-bootstrap/esm/Alert'
// import { index } from 'd3'
// import data from './data.json'

export const Relations = () => {

  const { newsN, queryBody, setQueryBody } = useNewsContext()

  let history = useHistory()

  const { hits } = newsN.hits
  const newsSource = hits.reduce((acc, current) => {
    acc.push(current._source)
    return acc
  }, [])

  // console.log(news.slice(0, 500).length)
  const news = newsSource.length > 300 ? newsSource.slice(0, 300) : newsSource
  // console.log(news.filter(el => el.hasOwnProperty('firm')))


  // const uaSort = (s1, s2) => s1.localeCompare(s2)

  const personFilterReducer = (accum, current) => {
    let resArr = [...accum, ...current.person.split(', ')].filter(el => el !== undefined)
    // return [...new Set(resArr)].sort(uaSort)
    return [...new Set(resArr)]
  }


  const uniquePersons = () => {
    return news
      .filter(el => el.hasOwnProperty('person'))
      .reduce(personFilterReducer, [])
    // .map((el, i) => el ? <option value={el} key={i}>{el}</option> : null)
  }

  const createSourceLinkArr = uniquePersons().reduce((acc, current, _, sArr) => {
    const fA = sArr.filter(el => el !== current)
    fA.forEach(el => acc.push({ source: current, target: el, weight: 0 }))
    return acc
  }, [])

  const modifiedNewsPersonsArray = news
    .filter(el => el.hasOwnProperty('person'))
    .map(el => el.person.split(', '))

  const sourceLinksArr = createSourceLinkArr.map(element => {
    // debugger
    modifiedNewsPersonsArray.forEach(el => {
      if (el.includes(element.source) && el.includes(element.target)) {
        element.weight++
      }
    })
    return element
  })

  var links = sourceLinksArr.filter(el => el.weight >= 3) // DATA FOR GRAPH - !LINKS!

  var nodesArray = links.reduce((acc, current) => {
    if (!acc.includes(current.source)) {
      acc.push(current.source)
    }
    return acc
  }, [])

  const nodes = nodesArray.reduce((acc, current) => { // DATA FOR GRAPH - !NODES!
    acc.push({ id: current, group: 1 })
    return acc
  }, [])

  const nodeClickHandler = data => {
    
    const detailedQueryBody = {
      ...queryBody,
      query: {
        ...queryBody.query,
        query_string: {
          ...queryBody.query.query_string,
          query: `${queryBody.query.query_string.query} person:"${data}"`
        }
      }
    }

    // console.log('FROM RELATIONS PERSONS', detailedQueryBody)
    setQueryBody(detailedQueryBody)
    history.push("/main")
  }

  return (
    <div className="col-md-10 ">
      <Container className="app-height" fluid>
        <Container fluid>
          <div className="relations-component">
            <Alert variant="danger" style={{ fontSize: "1.5rem" }}>
              Деякі функціональні можливості - недоступні. Розділ знаходиться у розробці.
            </Alert>
            <h3>Зв'язки персон за запитом</h3>
            <Graph data={{ nodes, links }} handler={nodeClickHandler} />
          </div>
        </Container>
      </Container>
    </div>
  )
}