import React from 'react'
import Button from 'react-bootstrap/esm/Button'
import Modal from 'react-bootstrap/esm/Modal'

export const DeleteConfirm = (props) => {

  const { show, onHide, confirm } = props

  return (
    <>
      <Modal show={show} onHide={onHide}>
        <Modal.Header >
          <Modal.Title>Підтвердіть видалення...</Modal.Title>
        </Modal.Header>
        {/* <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body> */}
        <Modal.Footer>
          <Button variant="success" onClick={() => confirm(false)}>
            НІ
          </Button>
          <Button variant="danger" onClick={() => confirm(true)}>
            ТАК
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}