import React, { useEffect, useState, useCallback } from 'react'
import './AdvancedSearch.scss'
import Container from 'react-bootstrap/esm/Container'
import Row from 'react-bootstrap/esm/Row'
import Col from 'react-bootstrap/esm/Col'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { useQuery } from '../hooks/query.hook'
import DropdownMultiselect from "react-multiselect-dropdown-bootstrap"
import DatePicker, { registerLocale } from 'react-datepicker'
import Accordion from 'react-bootstrap/Accordion'
import cloneDeep from 'lodash/cloneDeep'
import "react-datepicker/dist/react-datepicker.css"
import moment from 'moment'
import 'moment/locale/uk'
import uk from "date-fns/locale/uk"
import { useDispatch } from 'react-redux'
import { SET_QUERY_MODEL } from '../constants'

registerLocale("uk", uk);

export const AdvancedSearch = ({ setFilterCriteria, setFilterCriteriaValue, news, search }) => {

  const dispatch = useDispatch()
  const setQueryModel = (payload) => {
    dispatch({type: SET_QUERY_MODEL, payload: payload})
  }

  const [personOption, setPersonOption] = useState('')
  const [queryInputObject, setQueryInputObject] = useState({
    pubDate: {
      gte: moment().format('YYYY-MM-DD'),
      lte: moment().format('YYYY-MM-DD')
    }
  })
  const [startDate, setStartDate] = useState(new Date())
  const [endDate, setEndDate] = useState(new Date())
  const { createStringQuery } = useQuery()

  const filterFormHandler = (event) => {
    setFilterCriteria(event.target.name)
    setFilterCriteriaValue(event.target.value)
    // console.log(news)
  }

  const searchButtonHandler = (e) => {
    e.preventDefault()
    // console.log(queryInputObject)
    setQueryModel(queryInputObject)
    // console.log('QUERY MODEL STATE FROM ADVANCED SEARCH', queryModel);
    // console.log(createQuery(queryInputObject))
    // search(createQuery(queryInputObject))
    search(createStringQuery(queryInputObject))
  }

  const uaSort = (s1, s2) => s1.localeCompare(s2)

  const personFilterReducer = useCallback((accum, current) => {
    let resArr = [...accum, ...current.person.split(', ')].filter(el => el !== undefined)
    return [...new Set(resArr)].sort(uaSort)
  }, [])

  const createPersonOptionList = useCallback(() => {
    return news
      .filter(el => el.hasOwnProperty('person'))
      .reduce(personFilterReducer, [])
      .map((el, i) => el ? <option value={el} key={i}>{el}</option> : null)
  }, [news, personFilterReducer])

  const inputHandler = event => {
    if (event.target.value.length === 0 && event.target.name in queryInputObject) {
      const cloneQueryInputObj = cloneDeep(queryInputObject)
      delete cloneQueryInputObj[event.target.name]
      setQueryInputObject(cloneQueryInputObj)
    }
    else if (event.target.name === 'user') {
      setQueryInputObject({
        ...queryInputObject,
        [event.target.name]: event.target.value.split(',')
      })
    } else {
      setQueryInputObject({
        ...queryInputObject,
        [event.target.name]: event.target.value
      })
    }
  }

  const inputSourceHandler = selected => {
    if (selected.length === 0 && 'source' in queryInputObject) {
      const cloneQueryInputObj = cloneDeep(queryInputObject)
      delete cloneQueryInputObj.source
      setQueryInputObject(cloneQueryInputObj)
    } else {
      setQueryInputObject({
        ...queryInputObject,
        source: selected
      })
    }
  }

  const inbputStartDateHandler = (date) => {
    setStartDate(date)
    setQueryInputObject({
      ...queryInputObject,
      pubDate: {
        gte: moment(date).format('YYYY-MM-DD'),
        lte: moment(endDate).format('YYYY-MM-DD')
      }
    })
    console.log(moment(date).format('YYYY-MM-DD'))
  }

  const inbputEndDateHandler = (date) => {
    setEndDate(date)
    setQueryInputObject({
      ...queryInputObject,
      pubDate: {
        gte: moment(startDate).format('YYYY-MM-DD'),
        lte: moment(date).format('YYYY-MM-DD')
      }
    })
  }

  useEffect(() => {
    setPersonOption(createPersonOptionList())
  }, [news, createPersonOptionList])

  return (
    <Container fluid >
      <Row className="advanced-search">
        <Col>


          <Accordion defaultActiveKey="0">
            <Accordion.Toggle as={Button} variant="link" eventKey="0" className="shadow-none">
              <h5>Розширений пошук</h5>
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="0">
              <Form.Group>
                <Form.Label>&diams; <b><i>пошуковий запит: </i></b></Form.Label>
                <Form.Control
                  type="text"
                  name="textBody"
                  onChange={inputHandler}
                  placeholder="Введіть пошуковий вираз"
                  size="sm"
                />
                <Form.Label>&diams; <b><i>у назві документу: </i></b></Form.Label>
                <Form.Control
                  type="text"
                  name="title"
                  onChange={inputHandler}
                  placeholder="Введіть пошуковий вираз"
                  size="sm"
                />
                <Form.Label>&diams; <b><i>за джерелом:</i></b></Form.Label>
                <DropdownMultiselect
                  options={[
                    "Telegram",
                    "Twitter",
                    "YouTube",
                    "CONT",
                    "Weibo",
                    "Facebook",
                    "LiveJournal",
                    "LiveInternet",
                    "VK",
                    "Ok.ru",
                    "Reddit",
                    "Instagram",
                    "Medium.com",
                    "RuTube"
                  ]}
                  name="source"
                  placeholder="Оберіть джерело"
                  handleOnChange={inputSourceHandler}
                  onClick={e => e.preventDefault()}
                />
                <Form.Label>&diams; <b><i>за користувачем: </i></b></Form.Label>
                <Form.Control
                  type="text"
                  name="user"
                  onChange={inputHandler}
                  placeholder="Введіть чз кому"
                  size="sm"
                />
                <Form.Label>&diams; <b><i>події: </i></b></Form.Label>
                <Form.Control as="select" size="sm" name="event" onChange={inputHandler}>
                  <option value={''} >Не обрано</option>
                  <option value={'Преступление'}>Злочин</option>
                  <option value={'Нарушение законодательства'}>Порушення законодавства</option>
                  <option value={'Встреча, совещание, форум'}>Зустріч, нарада, форум</option>
                  <option value={'Выставка, семинар, фестиваль'}>Виставка, семінар, фестиваль</option>
                  <option value={'Визит'}>Візит</option>
                  <option value={'Разговор'}>Розмова, обговорення</option>
                  <option value={'Переговоры'}>Переговори</option>
                  <option value={'Слушание, дебаты'}>Слухання, дебати</option>
                  <option value={'Заключение договора'}>Укладання договору</option>
                  <option value={'Конфликты'}>Конфлікти</option>
                  <option value={'Протесты, забастовки, митинги'}>Протести, мітинги</option>
                  <option value={'Теракт, взрыв'}>Теракти</option>
                  <option value={'Катастрофа'}>Катастрофа</option>
                  <option value={'Спецоперации'}>Спецоперації</option>
                  <option value={'Убийство, покушение'}>Вбивство, замах</option>
                  <option value={'Перестрелка, вооруженное столкновение'}>Збройне зіткнення</option>
                  <option value={'Испытания'}>Випробування</option>
                  <option value={'Вынесение приговора'}>Винесення вироку</option>
                </Form.Control>
                <Form.Label>&diams; <b><i>виявлені персони: </i></b></Form.Label>
                <Form.Control
                  type="text"
                  name="person"
                  onChange={inputHandler}
                  placeholder="Введіть прізвище та/або ім&#39;я"
                  size="sm"
                />
                <Form.Label>&diams; <b><i>за датою: </i></b></Form.Label>
                <br />
                <DatePicker
                  dateFormat="dd.MM.yyyy"
                  selected={startDate}
                  onChange={inbputStartDateHandler}
                  locale='uk'
                  className="form-control form-control-sm"
                  closeOnScroll={true}
                  selectsStart
                  startDate={startDate}
                  endDate={endDate}
                />
                <DatePicker
                  dateFormat="dd.MM.yyyy"
                  selected={endDate}
                  onChange={inbputEndDateHandler}
                  locale='uk'
                  className="form-control form-control-sm"
                  closeOnScroll={true}
                  selectsEnd
                  startDate={startDate}
                  endDate={endDate}
                  minDate={startDate}
                />
                <br />
                <br />
                <Button
                  className="btn-outline-success advanced-search-button"
                  variant="primary"
                  onClick={searchButtonHandler}
                >
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                  </svg>
                  {/* Пошук */}
                </Button>
              </Form.Group>
            </Accordion.Collapse>
          </Accordion>



        </Col>
      </Row>
      <Row className="advanced-search">
        <Col>
          <Accordion defaultActiveKey="">
            <Accordion.Toggle as={Button} variant="link" eventKey="1" className="shadow-none">
              <h5>Фільтр результатів пошуку</h5>
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="1">
              <Form.Group>
                <Form.Label>&diams; за джерелом:</Form.Label>
                <Form.Control as="select" size="sm" name="source" onChange={filterFormHandler}>
                  <option value={'1'} >Усі</option>
                  {
                    news.reduce((accum, current) => {
                      return [...new Set([...accum, current.source])]
                    }, []).map((el, i) => el ? <option value={el} key={i}>{el} ({news.filter(e => e.source === el).length})</option> : null)
                  }
                </Form.Control>
                <Form.Label>&diams; за емоційним забарвленням:</Form.Label>
                <Form.Control as="select" size="sm" name="em" onChange={filterFormHandler}>
                  <option value={'1'} >Усі</option>
                  {
                    news.reduce((accum, current) => {
                      return [...new Set([...accum, current.em])]
                    }, []).map((el, i) => el ? <option value={el} key={i}>{el} ({news.filter(e => e.em === el).length})</option> : null)
                  }
                </Form.Control>
                <Form.Label>&diams; за виявленими персонами:</Form.Label>
                <Form.Control as="select" size="sm" name="person" onChange={filterFormHandler}>
                  <option value={'1'} >Усі</option>
                  {
                    personOption
                  }
                </Form.Control>
              </Form.Group>
            </Accordion.Collapse>
          </Accordion>


        </Col>
      </Row>
    </Container>
  )
}