import React, { useContext, useState } from 'react'
import DropdownButton from 'react-bootstrap/DropdownButton'
import Alert from 'react-bootstrap/esm/Alert'
import './SavedQueries.scss'
import Table from 'react-bootstrap/esm/Table'
import { useSelector, useDispatch } from 'react-redux'
import InputGroup from 'react-bootstrap/InputGroup'
import Button from 'react-bootstrap/esm/Button'
import FormControl from 'react-bootstrap/FormControl'
import { useHttp } from '../hooks/http.hook'
import { AuthContext } from '../context/AuthContext'
import { SET_QUERIES, SET_QUERY_MODEL } from '../constants'
import OverlayTrigger from 'react-bootstrap/esm/OverlayTrigger'
import Tooltip from 'react-bootstrap/esm/Tooltip'
import { FullTextSwitcher } from './FullTextSwitcher/FullTextSwitcher'

export const SavedQueries = ({ search }) => {

  const { request } = useHttp()
  const { token } = useContext(AuthContext)
  const currentQuery = useSelector(state => state.query)
  const queries = useSelector(state => state.queriesList)
  const dispatch = useDispatch()
  const [queryTitle, setQueryTitle] = useState('')

  const onSelectHandler = (elId) => (_) => {
    const [queryElement] = queries.filter(el => el._id === elId)
    if (queryElement.query) {
      dispatch({ type: SET_QUERY_MODEL, payload: { textBody: queryElement.query.query.query_string.query } })
      search(queryElement.query)
    } else {
      console.log(queryElement.query ? 'yes' : 'no')
    }
  }

  const queryTitleInputHandler = (event) => {
    if (event.target.value.trim().length > 0) {
      setQueryTitle(event.target.value)
    }

  }

  const saveQueryButtonHandler = async event => {
    event.preventDefault()
    try {

      const data = await request(
        '/api/query/create',
        'POST',
        {
          name: queryTitle,
          query: currentQuery
        },
        {
          Authorization: `Bearer ${token}`
        })
      // console.log('[FROM SAVED QUERIES STATE]', data)
      dispatch({
        type: SET_QUERIES,
        payload: [
          ...queries,
          data
        ]
      })

    } catch (e) { console.log('[FROM SAVED QUERIES SAVE BUTTON]', e) }
  }

  const updateQueryButtonHandler = elId => async event => {
    event.preventDefault()
    const [queryElement] = queries.filter(el => el._id === elId)
    try {
      const data = await request(
        '/api/query/update',
        'POST',
        {
          ...queryElement,
          query: currentQuery
        },
        {
          Authorization: `Bearer ${token}`
        }
      )
      const updatedQueries = queries.map(el => el._id === elId ? { ...el, query: data.query } : el)
      dispatch({
        type: SET_QUERIES,
        payload: updatedQueries
      })
    } catch (error) {

    }
    // console.log('[UPDATE QUERY]', updatedQueries)
  }

  return (
    <DropdownButton
      // onSelect={onSelectHandler}
      // as={ButtonGroup}
      className="saved-queries"
      key={'left'}
      id={`personal-sources`}
      drop={'left'}
      variant="secondary"
      title={` Персональні запити `}
    >
      {
        queries.length > 0
          ? <div>
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Назва</th>
                  <th>Запит</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {
                  queries.map((el, index) => {
                    const { name, query } = el
                    // console.log(el)
                    return (
                      // <Dropdown.Item eventKey={el._id} > </Dropdown.Item>
                      <tr key={el._id} >
                        <td>{index + 1}.</td>
                        <td>{name}</td>
                        <td>
                          <FullTextSwitcher text={query.query.query_string.query} />
                        </td>
                        <td>
                          <OverlayTrigger
                            placement="top"
                            overlay={
                              <Tooltip id="top">
                                Змінити на поточний запит
                              </Tooltip>
                            }
                          >
                            <button
                              className="btn btn-outline-success btn-sm my-2 my-sm-0"
                              type="submit"
                              onClick={updateQueryButtonHandler(el._id)}
                            >
                              <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" className="bi bi-save2" viewBox="0 0 16 16">
                                <path d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v4.5h2a.5.5 0 0 1 .354.854l-2.5 2.5a.5.5 0 0 1-.708 0l-2.5-2.5A.5.5 0 0 1 5.5 6.5h2V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z" />
                              </svg>
                            </button>
                          </OverlayTrigger>
                        </td>
                        <td>
                          <OverlayTrigger
                            placement="top"
                            overlay={
                              <Tooltip id="top">
                                Перейти
                              </Tooltip>
                            }
                          >
                            <button
                              className="btn btn-outline-success btn-sm my-2 my-sm-0"
                              type="submit"
                              onClick={onSelectHandler(el._id)}
                            >
                              <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                              </svg>
                            </button>
                          </OverlayTrigger>
                        </td>
                      </tr>
                    )
                  })
                }
              </tbody>
            </Table>
          </div>
          : <Alert variant="info">Запити відсутні</Alert>
      }
      <hr />
      <h5>Збереження поточного запиту</h5>
      <InputGroup className="mb-3 header-save-query-input">
        <FormControl
          placeholder="Назва запиту"
          aria-label="Назва запиту"
          aria-describedby="basic-addon2"
          onChange={queryTitleInputHandler}
        />
        <Button
          variant="outline-secondary"
          id="button-addon2"
          onClick={saveQueryButtonHandler}
        >
          Зберегти
        </Button>
      </InputGroup>
    </DropdownButton>
  )
}