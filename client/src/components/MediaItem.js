import React, { useState } from 'react'
import Media from 'react-bootstrap/Media'
import './MediaItem.scss'
import Container from 'react-bootstrap/esm/Container'
import Row from 'react-bootstrap/esm/Row'
import Col from 'react-bootstrap/esm/Col'
import { NewsModal } from './NewsModal/NewsModal'
import moment from 'moment'
import 'moment/locale/uk'
import uk from "date-fns/locale/uk"
import { registerLocale } from 'react-datepicker'
import Badge from 'react-bootstrap/esm/Badge'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Tooltip from 'react-bootstrap/Tooltip'
import { socMediaDefine } from '../helpers/socMediaDefine'

registerLocale("uk", uk);

export const MediaItem = ({ props }) => {

  const [modalShow, setModalShow] = useState(false)

  const { el } = props

  const isImagesExists = element => {
    return element.hasOwnProperty("img") ? true : false
  }

  const createTelegramLink = sourceLink => {
    const telegramMsgId = sourceLink.split('@')[1]
    return `https://t.me/s/${telegramMsgId}`
  }

  return (
    <Media className="media-custom">
      <Media.Body>
        {/* <h5 className="media-header" onClick={() => setModalShow(true)}>{el.title}</h5> */}
        <Container>
          <Row>
            <Col>
              <OverlayTrigger
                key='right'
                placement='right'
                overlay={
                  <Tooltip id='right'>
                    {el.source}
                  </Tooltip>
                }
              >
                <a
                  href={el.source === 'Telegram' ? createTelegramLink(el.URL) : el.URL}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {
                    socMediaDefine(el.source)
                  }
                  {/* <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-telegram" viewBox="0 0 16 16">
                  <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.287 5.906c-.778.324-2.334.994-4.666 2.01-.378.15-.577.298-.595.442-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294.26.006.549-.1.868-.32 2.179-1.471 3.304-2.214 3.374-2.23.05-.012.12-.026.166.016.047.041.042.12.037.141-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8.154 8.154 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629.093.06.183.125.27.187.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.426 1.426 0 0 0-.013-.315.337.337 0 0 0-.114-.217.526.526 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09z" />
                </svg> */}
                </a>
              </OverlayTrigger>
            </Col>
            <Col lg={2}>
              {
                el.citation &&
                <OverlayTrigger
                  placement="top"
                  overlay={
                    <Tooltip id="top">
                      Цитата
                    </Tooltip>
                  }
                >
                  <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" fill="currentColor" className="bi bi-chat-left-quote-fill" viewBox="0 0 16 16">
                    <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H4.414a1 1 0 0 0-.707.293L.854 15.146A.5.5 0 0 1 0 14.793V2zm7.194 2.766a1.688 1.688 0 0 0-.227-.272 1.467 1.467 0 0 0-.469-.324l-.008-.004A1.785 1.785 0 0 0 5.734 4C4.776 4 4 4.746 4 5.667c0 .92.776 1.666 1.734 1.666.343 0 .662-.095.931-.26-.137.389-.39.804-.81 1.22a.405.405 0 0 0 .011.59c.173.16.447.155.614-.01 1.334-1.329 1.37-2.758.941-3.706a2.461 2.461 0 0 0-.227-.4zM11 7.073c-.136.389-.39.804-.81 1.22a.405.405 0 0 0 .012.59c.172.16.446.155.613-.01 1.334-1.329 1.37-2.758.942-3.706a2.466 2.466 0 0 0-.228-.4 1.686 1.686 0 0 0-.227-.273 1.466 1.466 0 0 0-.469-.324l-.008-.004A1.785 1.785 0 0 0 10.07 4c-.957 0-1.734.746-1.734 1.667 0 .92.777 1.666 1.734 1.666.343 0 .662-.095.931-.26z" />
                  </svg>
                </OverlayTrigger>
              }
              {/* {el.citation && `yes ${el.citation[0].acit}`} */}
            </Col>
          </Row>
          <Row><h6 className="media-header" onClick={() => setModalShow(true)}><b>{el.title}</b></h6></Row>
          <Row>
            {
              isImagesExists(el)
                ?
                <Col className="col-media-item-image col-div align-self-center" xl={2} sm={12} >
                  <div>
                    <img
                      // width={170}
                      // height={64}
                      className="mr-3"
                      src={el.img}
                      alt="Generic placeholder"
                    />
                  </div>
                </Col>
                : null
            }

            <Col className="align-self-center col-div" xl={isImagesExists(el) ? 10 : 12} >
              <div >
                <div className="short-news-text" dangerouslySetInnerHTML={{ __html: `${el.textBody.slice(0, isImagesExists(el) ? 200 : 250)}&#8230;` }} />
                {/* {el.textBody.slice(0, isImagesExists(el) ? 200 : 250)}&#8230; */}
                <Badge variant="info" onClick={() => setModalShow(true)} style={{ cursor: 'pointer' }}>Переглянути</Badge>
                <p>
                  {
                    moment().diff(moment(el.pubDate), "days") <= 2
                      ? moment(el.pubDate).locale('uk').fromNow()
                      : moment(el.pubDate).locale('uk').format('L')
                    // ? moment(el.pubDate.substring(0, el.pubDate.length - 1)).locale('uk').fromNow()
                    // : moment(el.pubDate.substring(0, el.pubDate.length - 1)).locale('uk').format('L')
                  }
                  &#8226;
                  Опубліковано користувачем
                  <span className="source-publ">
                  &#8226;
                    <a
                      href={el.source === 'Telegram' ? createTelegramLink(el.URL) : el.URL}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {el.user}
                    </a>
                  </span>
                </p>
              </div>
            </Col>
          </Row>
        </Container>
      </Media.Body>
      <NewsModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        element={el}
      />
    </Media>
  )
}