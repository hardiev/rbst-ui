import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { SET_QUERY_RESULT_SIZE } from '../../constants'
import { useMessage } from '../../hooks/message.hook'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/esm/Button'
import Toast from 'react-bootstrap/Toast'
import errorLogo from '../../images/error.png'


export const DefaultElResponseSize = () => {

  const [sizeInput, setSizeInput] = useState(0)
  const resultSize = useSelector(state => state.queryResultSize)
  const dispatch = useDispatch()
  const { message, show, removeMessage, textMsg } = useMessage()

  const changeSizeHandlerButton = () => {
    if (sizeInput) {
      dispatch({ type: SET_QUERY_RESULT_SIZE, payload: sizeInput })
    } else message('Тільки цифрові символи')
  }

  const changeSizeHandler = event => {
    const size = Number(event.target.value)
    if (size) {
      setSizeInput(size)
    } else {
      message('Тільки цифрові символи')
      setSizeInput(0)
    }
  }

  return (
    <div>
      <InputGroup size="sm" className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="inputGroup-sizing-sm">Розмір результатів пошуку: &nbsp; <b>{resultSize}</b></InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          aria-label="Small"
          aria-describedby="inputGroup-sizing-sm"
          placeholder="Встановити новий розмір"
          onChange={changeSizeHandler}
        />
        <InputGroup.Append>
          <Button
            variant="outline-success"
            onClick={changeSizeHandlerButton}
          >
            Встановити
          </Button>
        </InputGroup.Append>
      </InputGroup>
      <Toast
        className="error-toast"
        onClose={() => removeMessage()}
        show={show}
        autohide={true}
        delay={1500}
      >
        <Toast.Body>
          <img src={errorLogo} className="rounded mr-2" alt="" width="30" height="30" />
          {textMsg}
        </Toast.Body>
      </Toast>
    </div>
  )
}