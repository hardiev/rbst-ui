import React from 'react'
import Pagination from 'react-bootstrap/Pagination'
import './PaginationNews.scss'

export const PaginationNews = ({ newsItemsPerPage,
  totalNewsItems,
  paginate,
  prevPage,
  nextPage,
  currentPage,
  paginationChunk,
  setPaginationChunk
}) => {
  const pageNumbers = []
  const pagesArray = { 0: [] }

  for (let i = 1; i <= Math.ceil(totalNewsItems / newsItemsPerPage); i++) {
    pageNumbers.push(i)
  }

  // cerate page array
  var i = 0
  var j = 0
  while (i < Math.ceil(pageNumbers.length / 5)) {
    pagesArray[i] = pageNumbers.slice(j, j + 5)
    j = j + 5
    i++
  }

  const nextPaginationChunk = () => {
    setPaginationChunk((prev) => prev + 1)
  }

  const prevPaginationChunk = () => {
    setPaginationChunk((prev) => prev - 1)
  }

  const firstPage = () => {
    paginate(1)
    setPaginationChunk(0)
  }

  const lastPage = () => {
    paginate(pageNumbers.length)
    setPaginationChunk(Math.ceil(pageNumbers.length / 5) - 1)
  }

  return (
    <Pagination className="pagination-news">
      <Pagination.First
        disabled={currentPage <= 1 ? true : false}
        onClick={firstPage}
      >
        Перша сторінка
      </Pagination.First>
      <Pagination.Prev
        onClick={prevPage}
        disabled={currentPage <= 1 ? true : false}
      >
        Попередня сторінка
      </Pagination.Prev>
      <Pagination.Ellipsis
        onClick={prevPaginationChunk}
        disabled={paginationChunk <= 0 ? true : false}
      />
      {pagesArray[paginationChunk].map((el, i) => (
        <Pagination.Item
          active={currentPage === el ? true : false}
          id={i}
          key={i}
          onClick={() => paginate(el)}
        >
          {el}
        </Pagination.Item>
      ))}
      <Pagination.Ellipsis
        onClick={nextPaginationChunk}
        disabled={paginationChunk + 1 >= Math.ceil(pageNumbers.length / 5) ? true : false}
      />
      <Pagination.Next
        onClick={() =>  nextPage() }
        disabled={currentPage >= pageNumbers.length ? true : false}
      >
        Наступна сторінка
      </Pagination.Next>
      <Pagination.Last
        disabled={currentPage >= pageNumbers.length ? true : false}
        onClick={lastPage}
      >
        Остання сторінка
      </Pagination.Last>
    </Pagination>
  )
}