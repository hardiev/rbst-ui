import React, { createContext, useCallback, useContext, useEffect, useState } from 'react'
import { useHttp } from '../hooks/http.hook'
// import { useQuery } from '../hooks/query.hook'
import config from '../config/config'
// import moment from 'moment'
import 'moment/locale/uk'
import { useDispatch, useSelector } from 'react-redux'
import { SET_QUERY, SET_NEWS, SET_DEFAULT_USER_QUERY, SET_QUERY_MODEL } from '../constants'

const NewsContext = createContext()

export const useNewsContext = () => {
  return useContext(NewsContext)
}

export const NewsProvider = ({ children }) => {
  const [newsN, setNewsN] = useState({})
  // const { createStringQuery } = useQuery()
  const defaultUserQuery = useSelector(state => state.defaultUserQuery)
  const [queryBody, setQueryBody] = useState(null)
  const { loading, request } = useHttp()
  const userName = config.elasticUser
  const password = config.elasticPass
  const authString = `${userName}:${password}`
  const resultSize = useSelector(state => state.queryResultSize)
  const dispatch = useDispatch()
  const [defQueryMarker, setDefQueryMarker] = useState(false)

  const fetchNews = useCallback(async (localstorageData) => {
    try {
      try {
        var getDefaultQuery = await request(
          `/api/users/${localstorageData.userId}`,
          'GET',
          null,
          {
            Authorization: `Bearer ${localstorageData.token}`
          }
        )
        if (getDefaultQuery.defaultQuery) {
          dispatch({ type: SET_DEFAULT_USER_QUERY, payload: getDefaultQuery.defaultQuery })
        }
      } catch (error) {
        console.log('[NEWSCONTEXT WARNING]', error)
      }
      dispatch({ type: SET_QUERY, payload: queryBody || getDefaultQuery?.defaultQuery || defaultUserQuery })
      setDefQueryMarker(true)
      const fetched = await request(`${config.elasticURL}${config.elasticIndexes}/_search?size=${resultSize}`,
        'POST',
        queryBody || getDefaultQuery?.defaultQuery || defaultUserQuery,
        {
          Authorization: 'Basic ' + btoa(authString)
        }
      )
      dispatch({ type: SET_NEWS, payload: fetched })
      setNewsN(fetched)
    } catch (error) { throw error }
  }, [authString, request, queryBody, dispatch, resultSize])

  useEffect(() => {
    const storageName = 'userData'
    const localstorageData = JSON.parse(localStorage.getItem(storageName))
    if (localstorageData) {
      fetchNews(localstorageData)
    }
  }, [fetchNews])

  useEffect(() => {
    if (defQueryMarker) {
      dispatch({
        type: SET_QUERY_MODEL,
        payload: {
          textBody: defaultUserQuery.query.query_string.query
        }
      })
    }
    // eslint-disable-next-line
  }, [defQueryMarker])

  return (
    <NewsContext.Provider value={{ newsN, setNewsN, loading, queryBody, setQueryBody }}>
      {children}
    </NewsContext.Provider>
  )
}