import React, { createContext, useContext, useCallback, useEffect } from 'react'
import { useHttp } from '../hooks/http.hook'
import { AuthContext } from './AuthContext'
import { useDispatch } from 'react-redux'
import { SET_QUERIES } from '../constants'

const QueryContext = createContext()

export const useQueryContext = () => {
  return useContext(QueryContext)
}

export const QueryProvider = ({ children }) => {
  // const [queries, setQueries] = useState([])
  const { loading, request } = useHttp()
  const { token } = useContext(AuthContext)
  const dispatch = useDispatch()

  const fetchQueries = useCallback(async () => {
    try {
      const fetched = await request('/api/query', 'GET', null, {
        Authorization: `Bearer ${token}`
      })
      // setQueries(fetched)
      dispatch({ type: SET_QUERIES, payload: fetched })
    } catch (e) { }
  }, [token, request, dispatch])

  useEffect(() => {
    fetchQueries()
  }, [fetchQueries])

  return (
    // <QueryContext.Provider value={{ queries, setQueries, loading }} >
    <QueryContext.Provider value={{ loading }} >
      {children}
    </QueryContext.Provider>
  )
}