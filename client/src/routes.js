import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { MainPage } from './pages/MainPage'
// import { StatPage } from './pages/StatPage'
import { UserCabPage } from './pages/UserCabPage'
import { AuthPage } from './pages/AuthPage'
import { HelpPage } from './pages/HelpPage'
import { Aggregation } from './components/Aggregation/Aggregation'
import { Digest } from './components/Digest/Digest'
import { Maps } from './components/Maps/Maps'
import { Relations } from './components/Relations/Relations'
import { InformPortrait } from './components/InformPortrait/InformPortrait'

export const useRoutes = isAuthenticated => {
  if (isAuthenticated) {
    return (
      <Switch>
        <Route path="/main" exact >
          <MainPage />
        </Route>
        {/* <Route path="/stat" exact >
          <StatPage />
        </Route> */}
        <Route path="/aggregation" exact >
          <Aggregation />
        </Route>
        <Route path="/digest/:checkFetch" component={Digest} exact>
        </Route>
        <Route path="/maps" exact>
          <Maps />
        </Route>
        <Route path="/relations" exact>
          <Relations />
        </Route>
        <Route path="/informportrait" exact>
          <InformPortrait />
        </Route>
        <Route path="/usercab" >
          <UserCabPage />
        </Route>
        <Route path="/help" >
          <HelpPage />
        </Route>
        <Redirect to="/main" />
      </Switch>
    )
  }
  else {
    return (
      <Switch>
        <Route path="/" exact >
          <AuthPage />
        </Route>
        <Redirect to="/" />
      </Switch>
    )
  }

}