import moment from 'moment'
import {
  SET_QUERY_MODEL,
  SET_QUERY,
  SET_NEWS,
  SET_QUERIES,
  SET_TOKEN,
  SET_USERS_LIST,
  SET_QUERY_RESULT_SIZE,
  SET_DEFAULT_USER_QUERY,
  SET_HEADER_QUERY_INPUT
} from '../constants';

const today = moment().format('YYYY-MM-DD')

const initState = {
  token: '',
  news: {},
  query: {
    aggs: {
      docs_count_aggregate: {
        date_histogram: {
          field: "pubDate",
          calendar_interval: "day"
        }
      }
    },
    highlight: {
      fields: {
        textBody: {
          type: "unified"
        }
      }
    },
    query: {
      query_string: {
        default_field: "textBody",
        default_operator: "AND",
        minimum_should_match: "100%",
        query: ''
      }
    }
  },
  queriesList: [],
  queryModel: {
    textBody: 'Украина OR Україна',
    source: ['Telegram^5', 'Twitter', 'YouTube^2'],
    pubDate: {
      gte: today,
      lte: today
    }
  },
  usersList: [],
  queryResultSize: 2000,
  defaultUserQuery: {
    aggs: {
      docs_count_aggregate: {
        date_histogram: {
          field: "pubDate",
          calendar_interval: "day"
        }
      }
    },
    highlight: {
      fields: {
        textBody: {
          type: "unified"
        }
      }
    },
    query: {
      query_string: {
        default_field: "textBody",
        default_operator: "AND",
        query: `(Укра?н*)source:(Telegram^5 OR Twitter OR YouTube^2)pubDate:[${today} TO *]`
      }
    },
    headerQueryInput: ""
  }
}

const reducer = (state = initState, action) => {
  switch (action.type) {
    case SET_QUERY_MODEL:
      return ({
        ...state,
        queryModel: action.payload
      })

    case SET_QUERY:
      return ({
        ...state,
        query: action.payload
      })

    case SET_QUERIES:
      return ({
        ...state,
        queriesList: action.payload
      })

    case SET_NEWS:
      return ({
        ...state,
        news: action.payload
      })

    case SET_TOKEN:
      return ({
        ...state,
        token: action.payload
      })

    case SET_USERS_LIST:
      return ({
        ...state,
        usersList: action.payload
      })

    case SET_QUERY_RESULT_SIZE:
      return ({
        ...state,
        queryResultSize: action.payload
      })

    case SET_DEFAULT_USER_QUERY:
      return ({
        ...state,
        defaultUserQuery: action.payload
      })
    
    case SET_HEADER_QUERY_INPUT:
      return ({
        ...state,
        headerQueryInput: action.payload
      })

    default:
      return state
  }
}

export default reducer