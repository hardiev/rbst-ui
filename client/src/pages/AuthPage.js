import React, { useState, useEffect, useContext } from 'react'
import './AuthPage.scss'
import logo from '../images/musang.jpeg'
// import logo from '../images/rbstcup.png'
import errorLogo from '../images/error.png'
import { useHttp } from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'
import Toast from 'react-bootstrap/Toast'
import { AuthContext } from '../context/AuthContext'


export const AuthPage = () => {
  const auth = useContext(AuthContext)
  const { message, show, removeMessage, textMsg } = useMessage()
  const { loading, request, error, clearError } = useHttp()
  const [form, setForm] = useState({
    email: '',
    password: ''
  })

  useEffect(() => {
    message(error)
    clearError()
  }, [error, message, clearError])

  const changeHandler = event => {
    setForm({ ...form, [event.target.name]: event.target.value })
  }

  // const registerHandler = async () => {
  //   try {
  //     const data = await request('/api/auth/register', 'POST', { ...form })
  //     message(data.message)
  //   } catch (e) { }
  // }
  
  const loginHandler = async () => {
    try {
      const data = await request('/api/auth/login', 'POST', { ...form })
      auth.login(data.token, data.userId, data.email, data.role)
    } catch (e) { }
  }

  return (
    <div className="auth-container">
      <form className="auth-form">
        <img className="mb-4" src={logo} alt="" width="92" height="92" />
        <h1 className="h3 mb-3 font-weight-normal">Авторизація</h1>
        <label htmlFor="email" className="sr-only">Email address</label>
        <input
          type="email"
          id="email"
          className="form-control auth-form-control"
          placeholder="Email"
          name="email"
          value={form.email}
          autoFocus=""
          onChange={changeHandler}
        />
        <label htmlFor="password" className="sr-only">Password</label>
        <input
          type="password"
          id="password"
          className="form-control auth-form-control"
          placeholder="Пароль"
          name="password"
          value={form.password}
          onChange={changeHandler}
        />
        <div className="checkbox mb-3">
          <label>
            <input type="checkbox" value="remember-me" /> Запам&rsquo;ятати
          </label>
        </div>
        <button
          className="btn btn-lg btn-primary btn-block"
          onClick={loginHandler}
          disabled={loading}
          type="button"
        >
          Вхід
        </button>
        {/* <button
          className="btn btn-lg btn-primary btn-block"
          onClick={registerHandler}
          disabled={loading}
        >
          Реєстрація
        </button> */}

        <Toast
          className="error-toast"
          onClose={() => removeMessage()}
          show={show}
          autohide={true}
          delay={3000}
        >
          {/* <Toast.Header>
            <img src={errorLogo} className="rounded mr-2" alt="" width="30" height="30" />
            <strong className="mr-auto">Помилка!</strong>
          </Toast.Header> */}
          <Toast.Body>
            <img src={errorLogo} className="rounded mr-2" alt="" width="30" height="30" />
            {textMsg}
          </Toast.Body>
        </Toast>
        <p className="mt-5 mb-3 text-muted">&copy; OSINT department</p>
      </form>
    </div>
  )
}