import React from 'react'
import { Header } from '../components/Header'
import Container from 'react-bootstrap/esm/Container'
import Row from 'react-bootstrap/esm/Row'
import Col from 'react-bootstrap/esm/Col'
import Card from 'react-bootstrap/Card'
import Alert from 'react-bootstrap/Alert'
import './UserCabPage.scss'
import { SavedQueriesListAdmin } from '../components/SavedQueriesListAdmin'
import { UsersManage } from '../components/UsersManage/UsersManage'
import { DefaultElResponseSize } from '../components/DefaultElResponseSize/DefaultElResponseSize'
import { DefaultUserQuery } from '../components/DefaultUserQuery/DefaultUserQuery'

export const UserCabPage = () => {

  const storageName = 'userData'
  const data = JSON.parse(localStorage.getItem(storageName))

  return (
    <div className="col-md-10 ">
      {
        !data
          ? <Alert variant="danger">Необхідна авторизація!
            &nbsp;&nbsp;&nbsp;
            <a href="/" rel="noopener noreferrer">
              Авторизуватись...
            </a>
          </Alert>
          : <div><Header />
            <Container fluid>
              <Row sm={4} >
                <Col lg={6} >
                  <Row>
                    <div className="user-cab-info">
                      <Card>
                        <Card.Header as="h5">Кабінет користувача</Card.Header>
                        <Card.Body>
                          <Card.Subtitle className="mb-2 text-muted">{data.email}</Card.Subtitle>
                          <DefaultElResponseSize />
                        </Card.Body>
                      </Card>
                    </div>
                  </Row>
                  <Row>
                    <DefaultUserQuery />
                  </Row>
                  <Row>
                    <UsersManage />
                  </Row>
                </Col>
                <Col lg={6} >
                  <SavedQueriesListAdmin />
                </Col>
              </Row>
            </Container>
          </div>
      }
    </div>
  )
}