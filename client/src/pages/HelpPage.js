import React from 'react'
import { BrowserRouter, NavLink, Redirect, Route, Switch } from 'react-router-dom'
import { Annotation } from '../components/Help/Annotation'
import { Intro } from '../components/Help/Intro'
import { Datastore } from '../components/Help/Datastore';
import { Interaction } from '../components/Help/Interaction';
import './HelpPage.scss'

export const HelpPage = () => {

  return (
    <div className="col-md-10">

      <div className="container help-page">
        <div className="row">
          <div style={{ margin: "0 auto" }}>
            <h3>Тимчасовий посібник користувача</h3>
          </div>
        </div>
        <BrowserRouter>
          <div className="row">
            <div className="col-sm-3 col-md-3">
              <ul className="nav flex-column nav-help-page">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/help/annotation" >Анотація</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/help/intro">Вступ</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/help/datastore">Сховище даних</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/help/interaction">Взаємодія користувача</NavLink>
                </li>
              </ul>
            </div>
            <div className="col-sm-9 col-md-9">
              <Switch>
                <>
                  <div className="container help-page-container">
                    <Route path="/help/annotation" exact>
                      <Annotation />
                    </Route>
                    <Route path="/help/intro" exact>
                      <Intro />
                    </Route>
                    <Route path="/help/datastore" exact>
                      <Datastore />
                    </Route>
                    <Route path="/help/interaction" exact>
                      <Interaction />
                    </Route>
                    <Redirect to="/help/annotation" />
                  </div>
                </>
              </Switch>
            </div>
          </div>
        </BrowserRouter>
      </div>
    </div>
  )
}