import React from 'react'
import Container from 'react-bootstrap/Container'
import { Header } from '../components/Header'
import { MainAnalytics } from '../components/MainAnalytics'

export const StatPage = () => {

  return (
    <div className="col-md-10">
      <Header />
      <Container fluid className="app-height">
        <MainAnalytics />
      </Container>
    </div>
  )
}