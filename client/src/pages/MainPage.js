import React, { useState, useCallback, useLayoutEffect, useEffect } from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Badge from 'react-bootstrap/Badge'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import ListGroup from 'react-bootstrap/ListGroup'
import ProgressBar from 'react-bootstrap/ProgressBar'
import { Loader } from '../components/Loader'
import { Header } from '../components/Header'
import { MediaItem } from '../components/MediaItem'
import Alert from 'react-bootstrap/Alert'
import { AdvancedSearch } from '../components/AdvancedSearch'
import { PaginationNews } from '../components/PaginationNews'
import { useNewsContext } from '../context/NewsContext'
import { useForceUpdate } from '../hooks/forceUpdate.hook';
import Button from 'react-bootstrap/esm/Button'
import { useDispatch, useSelector } from 'react-redux'
import { useQuery } from '../hooks/query.hook'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { SET_HEADER_QUERY_INPUT } from '../constants'

export const MainPage = () => {
  const [news, setNews] = useState([])
  const [filterCriteria, setFilterCriteria] = useState('')
  const [filterCriteriaValue, setFilterCriteriaValue] = useState('')
  const [currentPage, setCurrentPage] = useState(1)
  const [newsItemsPerPage] = useState(9)
  const [paginationChunk, setPaginationChunk] = useState(0)
  const [errorBadQuery, setErrorBadQuery] = useState(false)
  const forceUpdate = useForceUpdate();
  const { newsN, setQueryBody, loading } = useNewsContext()
  const queryCurrent = useSelector(state => state.query)
  const queryDefault = useSelector(state => state.defaultUserQuery)
  const query = queryCurrent || queryDefault
  const queryModel = useSelector(state => state.queryModel)
  const { createStringQuery } = useQuery()
  const dispatch = useDispatch()

  // console.log('QUERY', query)

  const filterNews = (criteria, criteriaValue) => {
    return criteria && criteriaValue && criteriaValue !== '1'
      ? news.filter(el => {
        const crit = criteria
        const critVal = criteriaValue
        if (el.hasOwnProperty(criteria) && el[criteria].split(', ').length > 1 && criteria === 'person') {
          return el[criteria].split(', ').includes(criteriaValue)
        }
        return el[crit] === critVal
      })
      // ? news.filter(el => { console.log(criteria, criteriaValue); const crit = criteria; const critVal = criteriaValue;
      //    return el[crit] === critVal; })
      : news
  }

  // For pagination
  const indexOfLastNewsItem = currentPage * newsItemsPerPage
  const indexOfFirstNewsItem = indexOfLastNewsItem - newsItemsPerPage
  var currentNewsItems = filterNews(filterCriteria, filterCriteriaValue).slice(indexOfFirstNewsItem, indexOfLastNewsItem)

  // For pagination (change page)
  const scrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }
  const paginate = useCallback(pageNumber => {
    setCurrentPage(pageNumber)
    scrollTop()
  }, [])
  const prevPage = () => {
    setCurrentPage(currentPage - 1); console.log(currentPage)
    scrollTop()
  }
  const nextPage = () => {
    setCurrentPage(currentPage + 1)
    scrollTop()
  }

  // const createSearchString = useCallback(stringParams => {
  //   const domain = 'http://91.103.255.42:81/cgi-bin/JSON.pl?' // 'http://188.214.30.108:81/cgi-bin/JSON.pl?db=all&query=Україна'
  //   const queryString = Object.keys(stringParams).reduce((accum, current, index, array) => {
  //     return accum += index === 0 ? `${current}=${stringParams[current]}` : `&${current}=${stringParams[current]}`
  //   }, domain)
  //   return queryString
  // }, [])

  const sortNews = (order) => _ => {
    if (order === 'desc') {
      const sorted = news.sort(function (a, b) {
        // Turn your strings into dates, and then subtract them
        // to get a value that is either negative, positive, or zero.
        return new Date(b.pubDate) - new Date(a.pubDate);
      })
      setNews(sorted)
    } else {
      const sorted = news.sort(function (a, b) {
        // Turn your strings into dates, and then subtract them
        // to get a value that is either negative, positive, or zero.
        return new Date(a.pubDate) - new Date(b.pubDate);
      })
      setNews(sorted)
    }
    forceUpdate()
  }

  const getPercentage = (total, current) => {
    return current * 100 / total
  }

  const listGroupItemCreator = (news, el, i) => {
    // const test = news.reduce((accum, current) => {
    //   return [...new Set([...accum, current.source])]
    // }, [])
    // console.log(test.reduce((accum, current) => {

    // }, []))
    const sourceFiltered = news.filter(e => e.source === el).length
    const percentage = +getPercentage(news.length, sourceFiltered).toFixed(2)

    return (
      <ListGroup.Item value={el} key={i}>
        {el} <Badge variant="success">{`${sourceFiltered}`}</Badge> <Badge variant="secondary">{`${percentage}%`}</Badge>
        <ProgressBar variant="success" now={percentage} />
      </ListGroup.Item>
    )
  }

  // Reload Button Handler
  const reloadHandler = () => {
    setQueryBody(createStringQuery(queryModel))
  }

  // Place current query to Header input
  const placeToHeaderInputHandler = () => {
    console.log('FROM CURRENT QUERY BUTTON')
    dispatch({type: SET_HEADER_QUERY_INPUT, payload: query.query.query_string.query})
  }

  useEffect(() => {

  }, [news])

  useEffect(() => {
    paginate(1)
    setPaginationChunk(0)
  }, [filterCriteria, filterCriteriaValue, paginate])

  useLayoutEffect(() => {
    setCurrentPage(1)
    // fetchData(createSearchString(/*searchString)*/)
    if (newsN.status === 400) {
      setErrorBadQuery(true)
    }
    else if (Object.keys(newsN).length > 0) {
      setErrorBadQuery(false)
      const { hits } = newsN.hits
      const newsNew = hits.reduce((acc, current) => {
        if (current.highlight) {
          const newCurrentSource = {
            ...current._source,
            highlight: current.highlight
          }
          acc.push(newCurrentSource)
        } else {
          acc.push(current._source)
        }
        return acc
      }, [])

      const newsNewHighlight = newsNew.map(element => {
        let newsWithHighlight = ''
        let newElement = {}

        if (element.highlight) {
          const { textBody } = element.highlight
          newsWithHighlight = textBody.reduce((accum, current) => {
            const stringWithElTag = current
            const stringWithoutElTag = stringWithElTag.replace(/<em>|<\/em>/gi, '')
            accum = accum.replace(stringWithoutElTag, stringWithElTag)
            return accum
          }, element.textBody)
          newElement = {
            ...element,
            textBody: newsWithHighlight
          }
          return newElement
        } else {
          return element
        }
      })
      setNews(newsNewHighlight)

    }

  }, [/*fetchData,*/  /*searchString,*/ newsN])

  return (
    <div className="col-md-10 ">
      {/* <Header search={getInputSearchString} /> */}
      <Container fluid>
        {/* <Row className="justify-content-end"> */}
        <Row>
          <Header search={setQueryBody} />
        </Row>
        <hr style={{ marginTop: 0 }} />
      </Container>
      <Container fluid>
        <Row sm={2} >
          <Col sm={9} >
            {
              loading
                ? <Loader />
                : errorBadQuery
                  ? <Alert variant="danger">Некоректний запит. Перевірте синтаксис та спробуйте знову!</Alert>
                  :
                  currentNewsItems.length !== 0
                    ? <>
                      <h5>
                        <Badge variant="light" className="badge-order" >Сортування за датою:</Badge>
                        <Button variant="secondary" size="sm" className="badge-order button-order" onClick={sortNews('desc')}>
                          <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" className="bi bi-sort-down" viewBox="0 0 14 14">
                            <path d="M3.5 2.5a.5.5 0 0 0-1 0v8.793l-1.146-1.147a.5.5 0 0 0-.708.708l2 1.999.007.007a.497.497 0 0 0 .7-.006l2-2a.5.5 0 0 0-.707-.708L3.5 11.293V2.5zm3.5 1a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zM7.5 6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zm0 3a.5.5 0 0 0 0 1h3a.5.5 0 0 0 0-1h-3zm0 3a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1h-1z" />
                          </svg>
                        </Button>
                        <Button variant="secondary" size="sm" className="badge-order button-order" onClick={sortNews('asc')}>
                          <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" className="bi bi-sort-up" viewBox="0 0 14 14">
                            <path d="M3.5 12.5a.5.5 0 0 1-1 0V3.707L1.354 4.854a.5.5 0 1 1-.708-.708l2-1.999.007-.007a.498.498 0 0 1 .7.006l2 2a.5.5 0 1 1-.707.708L3.5 3.707V12.5zm3.5-9a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zM7.5 6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zm0 3a.5.5 0 0 0 0 1h3a.5.5 0 0 0 0-1h-3zm0 3a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1h-1z" />
                          </svg>
                        </Button>
                        <OverlayTrigger
                          /*trigger="click"*/
                          key="bottom"
                          placement="bottom"
                          overlay={
                            <Popover id={`popover-positioned-bottom`}>
                              <Popover.Content>
                                <Popover.Title as="h3">Загальна кількість повідомлень:&#160;<b>{news.length}</b></Popover.Title>
                                <ListGroup>
                                  {
                                    news.reduce((accum, current) => {
                                      return [...new Set([...accum, current.source])]
                                    }, []).map((el, i) => {
                                      return el
                                        ? listGroupItemCreator(news, el, i)
                                        : null
                                    })
                                  }
                                </ListGroup>
                              </Popover.Content>
                            </Popover>
                          }
                        >
                          <Badge variant="light" className="badge-news-amount" >Повідомлення:&#160;<Badge variant="secondary">{news.length}</Badge>&#8659;</Badge>
                        </OverlayTrigger>
                        <OverlayTrigger
                          // trigger='hover'
                          key="bottom-current-query"
                          placement="bottom"
                          overlay={
                            <Popover id={`popover-positioned-bottom-current-query`}>
                              <Popover.Content>
                                <p>
                                  {query.query.query_string.query}
                                </p>
                              </Popover.Content>
                            </Popover>
                          }
                        >
                          <CopyToClipboard text={query.query.query_string.query}>
                            <Badge variant="light" className="badge-news-amount" >Поточний запит&#160;
                              {/* onClick={() => { navigator.clipboard.writeText(query.query.query_string.query) }} */}
                              <Badge
                                variant="secondary"
                                onClick={placeToHeaderInputHandler}
                              >
                                Скопіювати/Головна пошукова форма
                              </Badge>&#8659;
                            </Badge>
                          </CopyToClipboard>
                        </OverlayTrigger>
                        <Button variant="secondary" size="sm" className="badge-reload button-order" onClick={reloadHandler}>
                          <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" className="bi bi-bootstrap-reboot" viewBox="0 0 16 16">
                            <path d="M1.161 8a6.84 6.84 0 1 0 6.842-6.84.58.58 0 1 1 0-1.16 8 8 0 1 1-6.556 3.412l-.663-.577a.58.58 0 0 1 .227-.997l2.52-.69a.58.58 0 0 1 .728.633l-.332 2.592a.58.58 0 0 1-.956.364l-.643-.56A6.812 6.812 0 0 0 1.16 8z" />
                            <path d="M6.641 11.671V8.843h1.57l1.498 2.828h1.314L9.377 8.665c.897-.3 1.427-1.106 1.427-2.1 0-1.37-.943-2.246-2.456-2.246H5.5v7.352h1.141zm0-3.75V5.277h1.57c.881 0 1.416.499 1.416 1.32 0 .84-.504 1.324-1.386 1.324h-1.6z" />
                          </svg>
                        </Button>
                      </h5>
                      {currentNewsItems.map((el, i) => {
                        return (
                          <MediaItem props={{ el }} key={i} />
                        )
                      })}
                    </>
                    : <Alert variant="warning">За вашим запитом повідомлення відсутні!</Alert>
            }
            {
              currentNewsItems.length !== 0 && !loading && !errorBadQuery
                ? <PaginationNews
                  newsItemsPerPage={newsItemsPerPage}
                  totalNewsItems={filterNews(filterCriteria, filterCriteriaValue).length}
                  paginate={paginate}
                  currentPage={currentPage}
                  prevPage={prevPage}
                  nextPage={nextPage}
                  paginationChunk={paginationChunk}
                  setPaginationChunk={setPaginationChunk}
                />
                : null
            }
          </Col>
          {
            // !loading
            // ? 
            <Col sm={3} >
              <AdvancedSearch
                news={news}
                setFilterCriteria={setFilterCriteria}
                setFilterCriteriaValue={setFilterCriteriaValue}
                search={setQueryBody}
              />
            </Col>
            // : null
          }

        </Row>
      </Container>

    </div>
  )
}