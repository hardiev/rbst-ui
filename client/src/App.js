import React, { Fragment } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './redux/store'
import { useRoutes } from './routes'
import { useAuth } from './hooks/auth.hook'
import { AuthContext } from './context/AuthContext'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { Navbar } from './components/Navbar'
import { QueryProvider } from './context/QueryContext'
import { Loader } from './components/Loader'
import { NewsProvider } from './context/NewsContext'

function App() {
  const { token, login, logout, userId, userEmail, ready } = useAuth()
  const isAuthenticated = !!token
  const routes = useRoutes(isAuthenticated)

  if (!ready) {
    return <Loader />
  }

  return (

    <Provider store={store}>
      <AuthContext.Provider value={{
        token, login, logout, userId, userEmail, isAuthenticated
      }}>
        <NewsProvider>
          <QueryProvider>
            <Router>
              {isAuthenticated
                ? <Fragment>
                  <Container fluid className="app-height">
                    <Row className="main-row">
                      <Col md="2">
                        <Navbar />
                      </Col>
                      {routes}
                    </Row>
                  </Container>
                </Fragment>
                : routes
              }
              {/* {!isAuthenticated ? routes : null} */}
            </Router>
          </QueryProvider>
        </NewsProvider>
      </AuthContext.Provider>
    </Provider>
  )
}

export default App;
